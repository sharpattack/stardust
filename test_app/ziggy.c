//
// Created by madahin on 25/10/23.
//

#include "stardust.h"

// C library headers
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <time.h>

// Linux headers
#include <fcntl.h> // Contains file controls like O_RDWR
#include <errno.h> // Error integer and strerror() function
#include <termios.h> // Contains POSIX terminal control definitions
#include <unistd.h> // write(), read(), close()
#include <signal.h> // signal
#include <sys/ioctl.h>

bool gl_running = true;
struct sd_handle_st gl_stardust_handle;

struct serial_handle
{
    int fd;
    const char* path;
};

static void sigHandler(int sig) {
    printf("Interrupt %d\n", sig);
    gl_running = false;
}

sd_status_e my_open(struct sd_handle_st* handle, void* args)
{
    struct serial_handle* serial_port = args;
    serial_port->fd = open(serial_port->path, O_RDWR);
    if (serial_port->fd < 0) {
        printf("Error %i from open: %s\n", errno, strerror(errno));
        return SD_E_KO;
    }

    // Create new termios struct, we call it 'tty' for convention
    struct termios tty;

    // Read in existing settings, and handle any error
    if(tcgetattr(serial_port->fd, &tty) != 0) {
        printf("Error %i from tcgetattr: %s\n", errno, strerror(errno));
        return SD_E_KO;
    }

    tty.c_cflag &= ~PARENB; // Clear parity bit, disabling parity (most common)
    tty.c_cflag &= ~CSTOPB; // Clear stop field, only one stop bit used in communication (most common)
    tty.c_cflag &= ~CSIZE; // Clear all bits that set the data size
    tty.c_cflag |= CS8; // 8 bits per byte (most common)
    tty.c_cflag &= ~CRTSCTS; // Disable RTS/CTS hardware flow control (most common)
    tty.c_cflag |= CREAD | CLOCAL; // Turn on READ & ignore ctrl lines (CLOCAL = 1)

    tty.c_lflag &= ~ICANON;
    tty.c_lflag &= ~ECHO; // Disable echo
    tty.c_lflag &= ~ECHOE; // Disable erasure
    tty.c_lflag &= ~ECHONL; // Disable new-line echo
    tty.c_lflag &= ~ISIG; // Disable interpretation of INTR, QUIT and SUSP
    tty.c_iflag &= ~(IXON | IXOFF | IXANY); // Turn off s/w flow ctrl
    tty.c_iflag &= ~(IGNBRK|BRKINT|PARMRK|ISTRIP|INLCR|IGNCR|ICRNL); // Disable any special handling of received bytes

    tty.c_oflag &= ~OPOST; // Prevent special interpretation of output bytes (e.g. newline chars)
    tty.c_oflag &= ~ONLCR; // Prevent conversion of newline to carriage return/line feed
    // tty.c_oflag &= ~OXTABS; // Prevent conversion of tabs to spaces (NOT PRESENT ON LINUX)

    tty.c_cc[VTIME] = 10;    // Wait for up to 1s (10 deciseconds), returning as soon as any data is received.
    tty.c_cc[VMIN] = 0;

    // Set in/out baud rate to be 115200
    cfsetispeed(&tty, B115200);
    cfsetospeed(&tty, B115200);

    // Save tty settings, also checking for error
    if (tcsetattr(serial_port->fd, TCSANOW, &tty) != 0) {
        printf("Error %i from tcsetattr: %s\n", errno, strerror(errno));
        return SD_E_KO;
    }

    handle->args = args;
    return SD_E_OK;
}

uint16_t my_available(struct sd_handle_st* handle)
{
    int bytes_available = 0;
    if (handle != NULL && handle->args != NULL)
    {
        ioctl(*((int*)handle->args), FIONREAD, &bytes_available);
    }
    return (uint16_t)bytes_available;
}

sd_status_e my_close(struct sd_handle_st* handle)
{
    if (handle != NULL && handle->args != NULL)
    {
        close(*((int *)handle->args));
    }
    return SD_E_OK;
}

uint16_t my_read(struct sd_handle_st* handle, uint8_t* buf, uint16_t buf_size, uint32_t* err)
{
    ssize_t rode = read(*((int*)handle->args), buf, buf_size);
    /*printf("Recv : ");
    for (uint16_t i=0; i < buf_size; ++i)
    {
        printf("%02X ", buf[i]);
    }
    printf("\n");*/
    if (rode < 0)
    {
        *err = errno;
        return 0;
    }
    return rode;
}

uint16_t my_write(struct sd_handle_st* handle, const uint8_t* buf, uint16_t buf_size, uint32_t* err)
{
    /*printf("Send : ");
    for (uint16_t i=0; i < buf_size; ++i)
    {
        printf("%02X ", buf[i]);
    }
    printf("\n");*/
    ssize_t written = write(*((int*)handle->args), buf, buf_size);
    if (written < 0)
    {
        *err = errno;
        return 0;
    }
    return written;
}

void my_sleep(uint32_t ms)
{
    struct timespec ts;
    int res;

    ts.tv_sec = ms / 1000;
    ts.tv_nsec = (ms % 1000) * 1000000;

    do {
        res = nanosleep(&ts, &ts);
    } while (res && errno == EINTR);
}

void my_event_handler_fn(const void* trame, uint32_t event)
{
    const struct sd_trame* sd_data = (const struct sd_trame*)trame;
    switch (event)
    {
        case ZDO_CONCENTRATOR_IND_CB:
        {
            uint8_t index_data_reply = 0;
            const uint16_t srcAddr = SD_READ_NEXT_2_BYTE(sd_data->data, index_data_reply);
            const uint64_t IEEEAddr = SD_READ_NEXT_8_BYTE(sd_data->data, index_data_reply);
            const uint8_t cost = SD_READ_NEXT_1_BYTE(sd_data->data, index_data_reply);

            sd_bind(&gl_stardust_handle, true, 4, IEEEAddr);
            break;
        }
        default:
        {
            break;
        }
    }
}


int main(int argc, char ** argv)
{
    struct serial_handle handle;

    if (argc > 1)
    {
        handle.path = argv[1];
    }
    else
    {
        handle.path = "/dev/ttyUSB0";
    }
    memset(&gl_stardust_handle, 0, sizeof(gl_stardust_handle));

    if (signal(SIGINT, sigHandler) == SIG_ERR)
    {
        printf("Failed to setup signal handling: SIGINT\n");
        return EXIT_FAILURE;
    }

    if (signal(SIGTERM, sigHandler) == SIG_ERR)
    {
        printf("Failed to setup signal handling: SIGTERM\n");
        return EXIT_FAILURE;
    }

    sd_set_driver(&gl_stardust_handle, my_open, my_read, my_write, my_available, my_close, my_sleep, my_event_handler_fn);

    if (sd_initialize(&gl_stardust_handle, &handle) != SD_E_OK)
    {
        printf("Failed to initialize stardust\n");
        return EXIT_FAILURE;
    }

    //test_sd(&stardust_handle);
    uint16_t capabilities;
    sd_ping(&gl_stardust_handle, &capabilities);
    printf("Capabilities: 0x%04X\n", capabilities);
    struct sd_version_st ver;
    sd_version(&gl_stardust_handle, &ver);
    struct sd_device_info_st di;
    sd_device_info(&gl_stardust_handle, &di);

    printf("Device status: %d\n", di.deviceState);

    if (argc > 2 && argv[2][0] == '1')
    {
        if (sd_start_end_device(&gl_stardust_handle, 0xbaca, true) != SD_E_OK)
        {
            printf("Failed to start end device\n");
            return 1;
        }
    }
    else
    {
        if (sd_start_coordinator(&gl_stardust_handle, 0xbaca, false) != SD_E_OK)
        {
            printf("Failed to start coordinator\n");
            return 1;
        }
    }

    sd_device_info(&gl_stardust_handle, &di);



    printf("Device status: %d\n", di.deviceState);

    sd_get_bind_table(&gl_stardust_handle, 0, 0);

    while (gl_running)
    {
        sd_update(&gl_stardust_handle);
        my_sleep(10);
    }

    sd_clean(&gl_stardust_handle);
    return EXIT_SUCCESS;
}