//
// Created by madahin on 25/10/23.
//

#ifndef STARDUST_STARDUST_H
#define STARDUST_STARDUST_H

#ifdef __cplusplus
extern "C"
{
#endif

#include "sd/commands.h"
#include "sd/structs.h"


/*----------------------------------------------*/
/*               Driver functions               */
/*----------------------------------------------*/
void sd_set_driver(struct sd_handle_st *, sd_serial_open_fn, sd_serial_read_fn, sd_serial_write_fn, sd_serial_available_fn,
                    sd_serial_close_fn, sd_sleep_fn, sd_event_handler_fn);

sd_status_e sd_initialize(struct sd_handle_st *, void *);

sd_status_e sd_update(struct sd_handle_st *);

sd_status_e sd_clean(struct sd_handle_st *);

sd_status_e sd_sleep(struct sd_handle_st *, uint32_t);

sd_status_e sd_start_coordinator(struct sd_handle_st *, uint16_t, bool);

sd_status_e sd_start_end_device(struct sd_handle_st *, uint16_t, bool);

#ifdef __cplusplus
}
#endif

#endif //STARDUST_STARDUST_H
