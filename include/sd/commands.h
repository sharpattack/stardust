//
// Created by madahin on 05/11/23.
//

#ifndef STARDUST_COMMANDS_H
#define STARDUST_COMMANDS_H

#include "sd/structs.h"

/*----------------------------------------------*/
/*               Protocol defines               */
/*----------------------------------------------*/

#define SD_CHECK_CMD_STATUS(trame) ((trame).data[0] == 0 ? SD_E_OK : SD_E_KO)
#define SD_ZSAPI_TIMEOUT   0x21

// Capabilities returned by SYS_PING
// @see Z-Stack Monitor and Test API §3.8.1.2
#define SD_MT_CAP_SYS      0x0001
#define SD_MT_CAP_MAC      0x0002
#define SD_MT_CAP_NWK      0x0004
#define SD_MT_CAP_AF       0x0008
#define SD_MT_CAP_ZDO      0x0010
#define SD_MT_CAP_SAPI     0x0020
#define SD_MT_CAP_UTIL     0x0040
#define SD_MT_CAP_DEBUG    0x0080
#define SD_MT_CAP_APP      0x0100
#define SD_MT_CAP_ZOAD     0x1000

// Special value for bind timeout
// @see Z-Stack Monitor and Test API §3.7.1.5
#define SD_ALLOW_BIND_STOP 0x00
#define SD_ALLOW_BIND_INF  0xFF

// Configuration identifier
// @see Zigbee Network Processor (ZNP) Interface §3.1.2.1
#define ZCD_NV_STARTUP_OPTION           0x03
#define ZCD_NV_EXT_PANID                0x2D
#define ZCD_NV_PRECFGKEY                0x62
#define ZCD_NV_PRECFGKEY_ENABLE         0x63
#define ZCD_NV_LOGICAL_TYPE             0x87
#define ZCD_NV_ZDO_DIRECT_CB            0x8F
#define ZCD_NV_PANID                    0x83
#define ZCD_NV_TC_LINK_KEY              0x0101

#define DEV_TYPE_COORDINATOR            0x00
#define DEV_TYPE_ROUTER                 0x01
#define DEV_TYPE_END_DEVICE	            0x02

// All channel mask
// @see Z-Stack Monitor and Test API §3.13.1.6
#define CHANNEL_MASK_11					0x0000800
#define CHANNEL_MASK_12					0x0001000
#define CHANNEL_MASK_13					0x0002000
#define CHANNEL_MASK_14					0x0004000
#define CHANNEL_MASK_15					0x0008000
#define CHANNEL_MASK_16					0x0010000
#define CHANNEL_MASK_17					0x0020000
#define CHANNEL_MASK_18					0x0040000
#define CHANNEL_MASK_19					0x0080000
#define CHANNEL_MASK_20					0x0100000
#define CHANNEL_MASK_21					0x0200000
#define CHANNEL_MASK_22					0x0400000
#define CHANNEL_MASK_23					0x0800000
#define CHANNEL_MASK_24					0x1000000
#define CHANNEL_MASK_25					0x2000000
#define CHANNEL_MASK_26					0x4000000
#define ALL_CHANNEL_MASK				0x7FFF800

#define STARTOPT_CLEAR_CONFIG           0x01
#define STARTOPT_CLEAR_STATE            0x02
#define STARTOPT_AUTO                   0x04

// Latency identifier
// @see Z-Stack Monitor and Test API §3.2.1.1
#define AF_ENDPOINT_LATENCY_NO_LATENCY  0x0
#define AF_ENDPOINT_LATENCY_FAST        0x1
#define AF_ENDPOINT_LATENCY_SLOW        0x2

// Commissioning modes
// @see Z-Stack Monitor and Test API §3.13.1.5
#define COMMISSIONING_MODE_INITIALIZATION 0x00
#define COMMISSIONING_MODE_TOUCHLINK      0x01
#define COMMISSIONING_MODE_NET_STEERING   0x02
#define COMMISSIONING_MODE_NET_FORMATION  0x04
#define COMMISSIONING_MODE_FIND_AND_BIND  0x08

// Same, but as event reply
// @see Z-Stack Monitor and Test API §3.13.2.1
#define BDB_COMMISSIONING_INITIALIZATION   0x00
#define BDB_COMMISSIONING_NWK_STEERING     0x01
#define BDB_COMMISSIONING_FORMATION        0x02
#define BDB_COMMISSIONING_FINDING_BINDING  0x03
#define BDB_COMMISSIONING_TOUCHLINK        0x04
#define BDB_COMMISSIONING_PARENT_LOST      0x05

// Commissioning event status
#define BDB_COMMISSIONING_SUCCESS                        0x00
#define BDB_COMMISSIONING_IN_PROGRESS                    0x01
#define BDB_COMMISSIONING_NO_NETWORK                     0x02
#define BDB_COMMISSIONING_TL_TARGET_FAILURE              0x03
#define BDB_COMMISSIONING_TL_NOT_AA_CAPABLE              0x04
#define BDB_COMMISSIONING_TL_NO_SCAN_RESPONSE            0x05
#define BDB_COMMISSIONING_TL_NOT_PERMITTED               0x06
#define BDB_COMMISSIONING_TCLK_EX_FAILURE                0x07
#define BDB_COMMISSIONING_FORMATION_FAILURE              0x08
#define BDB_COMMISSIONING_FB_TARGET_IN_PROGRESS          0x09
#define BDB_COMMISSIONING_FB_INITIATOR_IN_PROGRESS       0x0A
#define BDB_COMMISSIONING_FB_NO_IDENTIFY_QUERY_RESPONSE  0x0B
#define BDB_COMMISSIONING_FB_BINDING_TABLE_FULL          0x0C
#define BDB_COMMISSIONING_NETWORK_RESTORED               0x0D
#define BDB_COMMISSIONING_FAILURE                        0x0E

#define SHORT_ADDRESS_COORDINATOR			0x0000
#define ALL_ROUTER_AND_COORDINATOR			0xFFFC

#define ZDO_STATUS_HOLD              0x00 // Initialized - not started automatically
#define ZDO_STATUS_INIT              0x01 // Initialized - not connected to anything
#define ZDO_STATUS_NWK_DISC          0x02 // Discovering PAN's to join
#define ZDO_STATUS_NWK_JOINING       0x03 // Joining a PAN
#define ZDO_STATUS_NWK_REJOINING     0x04 // ReJoining a PAN, only for end-devices
#define ZDO_STATUS_END_DEVICE_UNAUTH 0x05 // Joined but not yet authenticated by trust center
#define ZDO_STATUS_END_DEVICE        0x06 // Started as device after authentication
#define ZDO_STATUS_ROUTER            0x07 // Device joined, authenticated and is a router
#define ZDO_STATUS_COORD_STARTING    0x08 // Started as ZigBee Coordinator
#define ZDO_STATUS_ZB_COORD          0x09 // Started as ZigBee Coordinator
#define ZDO_STATUS_NWK_ORPHAN        0x0A // Device has lost information about its parent

// @see Z-Stack Monitor and Test API §3.12.2.29
#define ZMAC_SUCCESS                 0x00
#define ZMAC_NO_BEACON               0xEA
#define ZMAC_INVALID_PARAMETER       0xE8

// TODO: Commands status response
// #define afStatus_SUCCESS            ZSuccess           /* 0x00 */
// #define afStatus_FAILED             ZFailure           /* 0x01 */
// #define afStatus_INVALID_PARAMETER  ZInvalidParameter  /* 0x02 */
// #define afStatus_MEM_FAIL           ZMemError          /* 0x10 */
// #define afStatus_NO_ROUTE           ZNwkNoRoute        /* 0xCD */

#define ZB_BINDING_ADDR                           0xFFFE

/*----------------------------------------------*/
/*             SUBCOMMANDE TYPES                */
/*----------------------------------------------*/
// @see Z-Stack Monitor and Test API §2.1.2
#define SUB_RES        0x00
#define SUB_SYS        0x01
#define SUB_MAC        0x02
#define SUB_NWK        0x03
#define SUB_AF         0x04
#define SUB_ZDO        0x05
#define SUB_SAPI       0x06
#define SUB_UTIL       0x07
#define SUB_DEBUG      0x08
#define SUB_APP_INT    0x09
#define SUB_APP_CONF   0x0F
#define SUB_GREENPOWER 0x15

/*----------------------------------------------*/
/*               COMMANDE TYPES                 */
/*----------------------------------------------*/
// @see Z-Stack Monitor and Test API §2.1.2
#define SYS_POLL       0x00
#define SYS_SREQ       0x02
#define SYS_AREQ       0x04
#define SYS_SRSP       0x06

/*----------------------------------------------*/
/*                COMMANDE ID                   */
/*----------------------------------------------*/

#define SET_COMMAND_ID(trame, cmd) \
{                                  \
    (trame).cmd0 = ((cmd) >> 8) & 0xff; \
    (trame).cmd1 = ((cmd)     ) & 0xff; \
}

#define GET_COMMAND_ID(trame) (((trame).cmd0 << 8) | ((trame).cmd1))

#define MAKE_COMMAND(cmd, subcmd, app) (((cmd) << 12) | ((subcmd) << 8) | ((app)))

// For all command ID, @see Z-Stack Monitor and Test
#define SYS_RESET_REQ                               MAKE_COMMAND(SYS_AREQ, SUB_SYS     , 0x00)
#define SYS_RESET_REQ_ACK                           MAKE_COMMAND(SYS_AREQ, SUB_SYS     , 0x80)
#define SYS_PING                                    MAKE_COMMAND(SYS_SREQ, SUB_SYS     , 0x01)
#define SYS_PING_ACK                                MAKE_COMMAND(SYS_SRSP, SUB_SYS     , 0x01)
#define SYS_VERSION                                 MAKE_COMMAND(SYS_SREQ, SUB_SYS     , 0x02)
#define SYS_VERSION_ACK                             MAKE_COMMAND(SYS_SRSP, SUB_SYS     , 0x02)
#define SYS_OSAL_NV_WRITE                           MAKE_COMMAND(SYS_SREQ, SUB_SYS     , 0x09)
#define SYS_OSAL_NV_WRITE_ACK                       MAKE_COMMAND(SYS_SRSP, SUB_SYS     , 0x09)
#define SYS_RANDOM                                  MAKE_COMMAND(SYS_SREQ, SUB_SYS     , 0x0C)
#define SYS_RANDOM_ACK                              MAKE_COMMAND(SYS_SRSP, SUB_SYS     , 0x0C)
#define SYS_SET_TX_POWER                            MAKE_COMMAND(SYS_SREQ, SUB_SYS     , 0x14)
#define SYS_SET_TX_POWER_ACK                        MAKE_COMMAND(SYS_SRSP, SUB_SYS     , 0x14)
#define UTIL_GET_DEVICE_INFO                        MAKE_COMMAND(SYS_SREQ, SUB_UTIL    , 0x00)
#define UTIL_GET_DEVICE_INFO_ACK                    MAKE_COMMAND(SYS_SRSP, SUB_UTIL    , 0x00)
#define UTIL_SET_SECLEVEL                           MAKE_COMMAND(SYS_SREQ, SUB_UTIL    , 0x04)
#define UTIL_SET_SECLEVEL_ACK                       MAKE_COMMAND(SYS_SRSP, SUB_UTIL    , 0x04)
#define ZB_START_REQUEST                            MAKE_COMMAND(SYS_SREQ, SUB_SAPI    , 0x00)
#define ZB_START_REQUEST_ACK                        MAKE_COMMAND(SYS_SRSP, SUB_SAPI    , 0x00)
#define ZB_BIND_DEVICE                              MAKE_COMMAND(SYS_SREQ, SUB_SAPI    , 0x01)
#define ZB_BIND_DEVICE_ACK                          MAKE_COMMAND(SYS_SRSP, SUB_SAPI    , 0x01)
#define ZB_ALLOW_BIND                               MAKE_COMMAND(SYS_SREQ, SUB_SAPI    , 0x02)
#define ZB_ALLOW_BIND_ACK                           MAKE_COMMAND(SYS_SRSP, SUB_SAPI    , 0x02)
#define ZB_SEND_DATA_REQUEST                        MAKE_COMMAND(SYS_SREQ, SUB_SAPI    , 0x03)
#define ZB_SEND_DATA_REQUEST_ACK                    MAKE_COMMAND(SYS_SRSP, SUB_SAPI    , 0x03)
#define ZB_READ_CONF                                MAKE_COMMAND(SYS_SREQ, SUB_SAPI    , 0x04)
#define ZB_READ_CONF_ACK                            MAKE_COMMAND(SYS_SRSP, SUB_SAPI    , 0x04)
#define ZB_WRITE_CONF                               MAKE_COMMAND(SYS_SREQ, SUB_SAPI    , 0x05)
#define ZB_WRITE_CONF_ACK                           MAKE_COMMAND(SYS_SRSP, SUB_SAPI    , 0x05)
#define ZB_PERMIT_JOINING_REQUEST                   MAKE_COMMAND(SYS_SREQ, SUB_SAPI    , 0x08)
#define ZB_PERMIT_JOINING_REQUEST_ACK               MAKE_COMMAND(SYS_SRSP, SUB_SAPI    , 0x08)
#define ZB_BIND_CONFIRM                             MAKE_COMMAND(SYS_AREQ, SUB_SAPI    , 0x81)
#define ZB_ALLOW_BIND_CONFIRM                       MAKE_COMMAND(SYS_AREQ, SUB_SAPI    , 0x82)
#define AF_REGISTER                                 MAKE_COMMAND(SYS_SREQ, SUB_AF      , 0x00)
#define AF_REGISTER_ACK                             MAKE_COMMAND(SYS_SRSP, SUB_AF      , 0x00)
#define AF_DATA_REQUEST                             MAKE_COMMAND(SYS_SREQ, SUB_AF      , 0x01)
#define AF_DATA_REQUEST_ACK                         MAKE_COMMAND(SYS_SRSP, SUB_AF      , 0x01)
#define AF_INCOMING_MSG                             MAKE_COMMAND(SYS_AREQ, SUB_AF      , 0x81)
#define ZDO_IEEE_ADDR_REQ                           MAKE_COMMAND(SYS_SREQ, SUB_ZDO     , 0x01)
#define ZDO_IEEE_ADDR_REQ_ACK                       MAKE_COMMAND(SYS_SRSP, SUB_ZDO     , 0x01)
#define ZDO_BIND_REQ                                MAKE_COMMAND(SYS_SREQ, SUB_ZDO     , 0x25)
#define ZDO_BIND_REQ_ACK                            MAKE_COMMAND(SYS_SRSP, SUB_ZDO     , 0x25)
#define ZDO_NWK_DISCOVERY_REQ                       MAKE_COMMAND(SYS_SREQ, SUB_ZDO     , 0x26)
#define ZDO_NWK_DISCOVERY_REQ_ACK                   MAKE_COMMAND(SYS_SRSP, SUB_ZDO     , 0x26)
#define ZDO_MGMT_NWK_DISC_REQ                       MAKE_COMMAND(SYS_SREQ, SUB_ZDO     , 0x30)
#define ZDO_MGMT_NWK_DISC_REQ_ACK                   MAKE_COMMAND(SYS_SRSP, SUB_ZDO     , 0x30)
#define ZDO_MGMT_BIND_REQ                           MAKE_COMMAND(SYS_SREQ, SUB_ZDO     , 0x33)
#define ZDO_MGMT_BIND_REQ_ACK                       MAKE_COMMAND(SYS_SRSP, SUB_ZDO     , 0x33)
#define ZDO_MGMT_PERMIT_JOIN_REQ                    MAKE_COMMAND(SYS_SREQ, SUB_ZDO     , 0x36)
#define ZDO_MGMT_PERMIT_JOIN_REQ_ACK                MAKE_COMMAND(SYS_SRSP, SUB_ZDO     , 0x36)
#define ZDO_MSG_CB_REGISTER_REQ                     MAKE_COMMAND(SYS_SREQ, SUB_ZDO     , 0x3E)
#define ZDO_MSG_CB_REGISTER_REQ_ACK                 MAKE_COMMAND(SYS_SRSP, SUB_ZDO     , 0x3E)
#define ZDO_STARTUP_FROM_APP                        MAKE_COMMAND(SYS_SREQ, SUB_ZDO     , 0x40)
#define ZDO_STARTUP_FROM_APP_ACK                    MAKE_COMMAND(SYS_SRSP, SUB_ZDO     , 0x40)
#define ZDO_STARTUP_FROM_APP_EX                     MAKE_COMMAND(SYS_SREQ, SUB_ZDO     , 0x54)
#define ZDO_IEEE_ADDR_RSP                           MAKE_COMMAND(SYS_AREQ, SUB_ZDO     , 0x81)
#define ZDO_BIND_RSP                                MAKE_COMMAND(SYS_AREQ, SUB_ZDO     , 0xA1)
#define ZDO_MGMT_NWK_DISC_RSP                       MAKE_COMMAND(SYS_AREQ, SUB_ZDO     , 0xB0)
#define ZDO_MGMT_BIND_RSP                           MAKE_COMMAND(SYS_AREQ, SUB_ZDO     , 0xB3)
#define ZDO_MGMT_PERMIT_JOIN_RSP                    MAKE_COMMAND(SYS_AREQ, SUB_ZDO     , 0xB6)
#define ZDO_STATE_CHANGE_IND                        MAKE_COMMAND(SYS_AREQ, SUB_ZDO     , 0xC0)
#define ZDO_END_DEVICE_ANNCE_IND                    MAKE_COMMAND(SYS_AREQ, SUB_ZDO     , 0xC1)
#define ZDO_SRC_RTG_IND                             MAKE_COMMAND(SYS_AREQ, SUB_ZDO     , 0xC4)
#define ZDO_BEACON_NOTIFY_IND                       MAKE_COMMAND(SYS_AREQ, SUB_ZDO     , 0xC5)
#define ZDO_NWK_DISCOVERY_CNF                       MAKE_COMMAND(SYS_AREQ, SUB_ZDO     , 0xC7)
#define ZDO_CONCENTRATOR_IND_CB                     MAKE_COMMAND(SYS_AREQ, SUB_ZDO     , 0xC8)
#define ZDO_TC_DEV_IND                              MAKE_COMMAND(SYS_AREQ, SUB_ZDO     , 0xCA)
#define ZDO_MSG_CB_INCOMING                         MAKE_COMMAND(SYS_AREQ, SUB_ZDO     , 0xFF)
#define APP_CNF_SET_ALLOWREJOIN_TC_POLICY           MAKE_COMMAND(SYS_SREQ, SUB_APP_CONF, 0x03)
#define APP_CNF_SET_ALLOWREJOIN_TC_POLICY_ACK       MAKE_COMMAND(SYS_SRSP, SUB_APP_CONF, 0x03)
#define APP_CNF_BDB_START_COMMISSIONING             MAKE_COMMAND(SYS_SREQ, SUB_APP_CONF, 0x05)
#define APP_CNF_BDB_START_COMMISSIONING_ACK         MAKE_COMMAND(SYS_SRSP, SUB_APP_CONF, 0x05)
#define APP_CNF_BDB_SET_CHANNEL                     MAKE_COMMAND(SYS_SREQ, SUB_APP_CONF, 0x08)
#define APP_CNF_BDB_SET_CHANNEL_ACK                 MAKE_COMMAND(SYS_SRSP, SUB_APP_CONF, 0x08)
#define APP_CNF_BDB_SET_TC_REQUIRE_KEY_EXCHANGE     MAKE_COMMAND(SYS_SREQ, SUB_APP_CONF, 0x09)
#define APP_CNF_BDB_SET_TC_REQUIRE_KEY_EXCHANGE_ACK MAKE_COMMAND(SYS_SRSP, SUB_APP_CONF, 0x09)
#define APP_CNF_BDB_ZED_ATTEMPT_RECOVER_NWK         MAKE_COMMAND(SYS_SREQ, SUB_APP_CONF, 0x09)
#define APP_CNF_BDB_ZED_ATTEMPT_RECOVER_NWK_ACK     MAKE_COMMAND(SYS_SRSP, SUB_APP_CONF, 0x09)
#define APP_CNF_BDB_COMMISSIONING_NOTIFICATION      MAKE_COMMAND(SYS_AREQ, SUB_APP_CONF, 0x80)


/*----------------------------------------------*/
/*              Protocol functions              */
/*----------------------------------------------*/

#pragma region MT_AF
sd_status_e sd_register_endpoint(struct sd_handle_st* handle, uint8_t, uint16_t, uint16_t, uint8_t, uint8_t, uint8_t, const uint16_t*, uint8_t, const uint16_t*);
void sd_evt_af_incoming_msg(struct sd_handle_st* handle, const struct sd_trame* callback);
#pragma endregion MT_AF

#pragma region MT_APP
#pragma endregion MT_APP

#pragma region MT_DEBUG
#pragma endregion MT_DEBUG

#pragma region MT_MAC
#pragma endregion MT_MAC

#define ZCD_NV_PANID                    0x83

#pragma region MT_SAPI
sd_status_e sd_start_request(struct sd_handle_st*);
sd_status_e sd_bind(struct sd_handle_st* handle, bool create, uint16_t commandId, uint64_t destination);
sd_status_e sd_send_data(struct sd_handle_st* handle, uint16_t destination, uint16_t commandID, uint8_t dataHandle, uint8_t ack, uint8_t radius, uint8_t dataLen, const uint8_t* data);
sd_status_e sd_send_data_to_endpoint(struct sd_handle_st* handle, uint16_t dstAddr, uint8_t dstEndpoint, uint8_t srcEndpoint, uint16_t clusterId, uint8_t transId, uint8_t options, uint8_t radius, uint8_t len, const uint8_t* data);
 sd_status_e sd_read_configuration(struct sd_handle_st*, uint8_t, struct sd_nvm_conf_st*);
sd_status_e sd_write_configuration(struct sd_handle_st*, uint8_t, const struct sd_nvm_conf_st*);
sd_status_e sd_set_startup_option(struct sd_handle_st*, uint8_t);
sd_status_e sd_set_ext_panid(struct sd_handle_st*, uint64_t);
sd_status_e sd_set_prec_cfgkey(struct sd_handle_st*, const sd_prefcfgkey_t);
sd_status_e sd_enable_prec_cfgkey(struct sd_handle_st*, uint8_t);
sd_status_e sd_set_device_type(struct sd_handle_st*, uint8_t);
sd_status_e sd_activate_direct_callback(struct sd_handle_st*, uint8_t);
sd_status_e sd_set_panid(struct sd_handle_st*, uint16_t);
sd_status_e sd_set_join_permission(struct sd_handle_st*, uint16_t, uint8_t);
void sd_evt_bind_confirm(struct sd_handle_st* handle, const struct sd_trame* callback);
void sd_evt_allow_bind_confirm(struct sd_handle_st* handle, const struct sd_trame* callback);
#pragma endregion MT_SAPI

#pragma region MT_SYS
sd_status_e sd_soft_reset(struct sd_handle_st*);
sd_status_e sd_hard_reset(struct sd_handle_st*);
sd_status_e sd_ping(struct sd_handle_st*, uint16_t*);
sd_status_e sd_version(struct sd_handle_st*, struct sd_version_st*);
sd_status_e sd_write_osal_nv(struct sd_handle_st*, uint16_t, uint8_t, uint8_t, const uint8_t*);
sd_status_e sd_write_tc_link_key(struct sd_handle_st*, const sd_tclinkkey_t);
sd_status_e sd_random(struct sd_handle_st*, uint16_t*);
sd_status_e sd_set_tx_power(struct sd_handle_st*, uint8_t*);
#pragma endregion MT_SYS

#pragma region MT_UTIL
sd_status_e sd_device_info(struct sd_handle_st*, struct sd_device_info_st*);
sd_status_e sd_allow_bind(struct sd_handle_st*, uint8_t);
sd_status_e sd_set_security_level(struct sd_handle_st*, uint8_t);
#pragma endregion MT_UTIL

#pragma region MT_ZDO
sd_status_e sd_request_IEEE_addr(struct sd_handle_st*, uint16_t, uint8_t, uint8_t);
sd_status_e sd_bind_request(struct sd_handle_st*, uint16_t, uint64_t, uint8_t, uint16_t, uint8_t, uint64_t, uint8_t);
sd_status_e sd_set_ZDO_callback(struct sd_handle_st*, uint16_t, uint8_t*);
sd_status_e sd_get_bind_table(struct sd_handle_st* handle, const uint16_t dstAddr, const uint8_t startIndex);
sd_status_e sd_discover_networks(struct sd_handle_st*, uint16_t, uint32_t, uint8_t);
sd_status_e sd_discover_networks2(struct sd_handle_st*, uint32_t, uint8_t);
sd_status_e sd_allow_join(struct sd_handle_st*, uint8_t, uint16_t, uint8_t, uint8_t);
sd_status_e sd_startup_from_app(struct sd_handle_st*, uint16_t);
sd_status_e sd_startup_from_app_ex(struct sd_handle_st*, uint8_t, uint8_t);
void sd_evt_zdo_state_change(struct sd_handle_st*, const struct sd_trame*);
void sd_evt_zdo_nwk_discovery(struct sd_handle_st*, const struct sd_trame*);
void sd_evt_zdo_beacon_notify(struct sd_handle_st*, const struct sd_trame*);
void sd_evt_zdo_concentrator_ind(struct sd_handle_st*, const struct sd_trame*);
void sd_evt_zdo_dev_annce_ind(struct sd_handle_st* handle, const struct sd_trame* trame);
void sd_evt_zdo_src_rtg_ind(struct sd_handle_st* handle, const struct sd_trame* trame);
void sd_evt_zdo_dev_ind(struct sd_handle_st* handle, const struct sd_trame* trame);
void sd_evt_zdo_msg_cb_incoming(struct sd_handle_st* handle, const struct sd_trame* trame);
#pragma endregion MT_ZDO

#pragma region MT_APPCONFIG
sd_status_e sd_trust_center_allow_join(struct sd_handle_st*, bool);
sd_status_e sd_commissioning(struct sd_handle_st*, uint8_t);
sd_status_e sd_set_channel(struct sd_handle_st*, bool, uint32_t);
sd_status_e sd_set_require_key_exchange(struct sd_handle_st*, bool);
sd_status_e sd_try_rejoin_previous_nwk(struct sd_handle_st* handle);
void sd_evt_commissioning(struct sd_handle_st*, const struct sd_trame*);
#pragma endregion MT_APPCONFIG

#if SD_DEV_MODE
const char* sd_get_cmd_name(const struct sd_trame* cmd);
#define GET_CMD_NAME(x) sd_get_cmd_name((x))
#else
#define GET_CMD_NAME(x) ""
#endif

#endif //STARDUST_COMMANDS_H
