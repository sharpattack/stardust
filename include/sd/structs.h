//
// Created by madahin on 05/11/23.
//

#ifndef STARDUST_STRUCTS_H
#define STARDUST_STRUCTS_H

#include <stdbool.h>
#include <inttypes.h>

/*----------------------------------------------*/
/*                Driver structs                */
/*----------------------------------------------*/

struct sd_trame
{
    uint8_t length;
    uint8_t cmd0;
    uint8_t cmd1;
    const uint8_t* data;
};

typedef enum
{
    SD_E_KO = 0,
    SD_E_OK = 1,
} sd_status_e;

enum sd_commissioning_status_e
{
    m,
};

struct sd_handle_st;

typedef sd_status_e (*sd_serial_open_fn)(struct sd_handle_st*, void*);
typedef uint16_t (*sd_serial_read_fn)(struct sd_handle_st*, uint8_t*, uint16_t, uint32_t*);
typedef uint16_t (*sd_serial_write_fn)(struct sd_handle_st*, const uint8_t*, uint16_t, uint32_t*);
typedef uint16_t (*sd_serial_available_fn)(struct sd_handle_st*);
typedef sd_status_e (*sd_serial_close_fn)(struct sd_handle_st*);
typedef void (*sd_sleep_fn)(uint32_t);
typedef void (*sd_event_handler_fn)(const void*, uint32_t);

struct sd_driver_st {
    sd_serial_open_fn      open;
    sd_serial_read_fn      read;
    sd_serial_write_fn     write;
    sd_serial_available_fn available;
    sd_serial_close_fn     close;
    sd_sleep_fn            sleep;
    sd_event_handler_fn    event_handler;
};

struct sd_handle_st {
    void* args;
    uint32_t status;
    uint16_t capabilities;
    struct sd_driver_st _driver;
    bool _driver_initialized;
    bool _isV3;
};

/*----------------------------------------------*/
/*               Protocol structs               */
/*----------------------------------------------*/
struct sd_version_st {
    uint8_t transport_rev;
    uint8_t product_id;
    uint8_t major_rel;
    uint8_t minor_rel;
    uint8_t maintenance_rel;
    uint32_t revision_num;
    uint8_t bootloader_buildtype;
};

struct sd_device_info_st {
    uint8_t status;
    uint64_t IEEEAddr;
    uint16_t shortAddr;
    uint8_t deviceType;
    uint8_t deviceState;
};

struct sd_conf_st {
    uint8_t status;
    uint64_t IEEEAddr;
    uint16_t shortAddr;
    uint8_t deviceType;
    uint8_t deviceState;
};

struct sd_nvm_conf_st {
    uint8_t length;
    uint8_t value[128];
};

typedef uint8_t sd_prefcfgkey_t[16];
typedef uint8_t sd_tclinkkey_t[32];

/*----------------------------------------------*/
/*                 Data macros                  */
/*----------------------------------------------*/
#define SD_WRITE_1_BYTE(array, index, data)                   \
    {(array)[(index)] = (uint8_t)(data);}

#define SD_WRITE_2_BYTE(array, index, data)                   \
    {(array)[(index)]     = (uint8_t)(((data) >>  0) & 0xFF); \
     (array)[(index + 1)] = (uint8_t)(((data) >>  8) & 0xFF);}

#define SD_WRITE_4_BYTE(array, index, data)                   \
    {(array)[(index)]     = (uint8_t)(((data) >>  0) & 0xFF); \
     (array)[(index + 1)] = (uint8_t)(((data) >>  8) & 0xFF); \
     (array)[(index + 2)] = (uint8_t)(((data) >> 16) & 0xFF); \
     (array)[(index + 3)] = (uint8_t)(((data) >> 24) & 0xFF);}

#define SD_WRITE_8_BYTE(array, index, data)                   \
    {(array)[(index)]     = (uint8_t)(((data) >>  0) & 0xFF); \
     (array)[(index + 1)] = (uint8_t)(((data) >>  8) & 0xFF); \
     (array)[(index + 2)] = (uint8_t)(((data) >> 16) & 0xFF); \
     (array)[(index + 3)] = (uint8_t)(((data) >> 24) & 0xFF); \
     (array)[(index + 4)] = (uint8_t)(((data) >> 32) & 0xFF); \
     (array)[(index + 5)] = (uint8_t)(((data) >> 40) & 0xFF); \
     (array)[(index + 6)] = (uint8_t)(((data) >> 48) & 0xFF); \
     (array)[(index + 7)] = (uint8_t)(((data) >> 56) & 0xFF);}

#define SD_WRITE_NEXT_1_BYTE(array, index, data)        \
    {SD_WRITE_1_BYTE((array), (index), (data));(index) += 1;}

#define SD_WRITE_NEXT_2_BYTE(array, index, data)        \
    {SD_WRITE_2_BYTE((array), (index), (data));(index) += 2;}

#define SD_WRITE_NEXT_4_BYTE(array, index, data)        \
    {SD_WRITE_4_BYTE((array), (index), (data));(index) += 4;}

#define SD_WRITE_NEXT_8_BYTE(array, index, data)        \
    {SD_WRITE_8_BYTE((array), (index), (data));(index) += 8;}

#define SD_READ_1_BYTE(array, index)        \
    ((uint8_t)((array)[(index)]))

#define SD_READ_2_BYTE(array, index) \
    (((uint16_t)((array)[(index + 1)]) << 8) |   \
      (uint16_t)((array)[(index)]))

#define SD_READ_4_BYTE(array, index) \
    (((uint32_t)((array)[(index + 3)]) << 24) |  \
     ((uint32_t)((array)[(index + 2)]) << 16) |  \
     ((uint32_t)((array)[(index + 1)]) <<  8) |  \
      (uint32_t)((array)[(index)]))

#define SD_READ_8_BYTE(array, index) \
    (((uint64_t)((array)[(index + 7)]) << 56) |  \
     ((uint64_t)((array)[(index + 6)]) << 48) |  \
     ((uint64_t)((array)[(index + 5)]) << 40) |  \
     ((uint64_t)((array)[(index + 4)]) << 32) |  \
     ((uint64_t)((array)[(index + 3)]) << 24) |  \
     ((uint64_t)((array)[(index + 2)]) << 16) |  \
     ((uint64_t)((array)[(index + 1)]) <<  8) |  \
      (uint64_t)((array)[(index)]))

#define SD_READ_NEXT_1_BYTE(array, index) \
    SD_READ_1_BYTE((array), (index)); (index) += 1;

#define SD_READ_NEXT_2_BYTE(array, index) \
    SD_READ_2_BYTE((array), (index)); (index) += 2;

#define SD_READ_NEXT_4_BYTE(array, index) \
    SD_READ_4_BYTE((array), (index)); (index) += 4;

#define SD_READ_NEXT_8_BYTE(array, index) \
    SD_READ_8_BYTE((array), (index)); (index) += 8;

#endif //STARDUST_STRUCTS_H
