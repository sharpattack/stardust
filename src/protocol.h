//
// Created by madahin on 29/10/23.
//

#ifndef STARDUST_PROTOCOL_H
#define STARDUST_PROTOCOL_H

#include <inttypes.h>
#include <string.h>

#include "stardust.h"
#include "stardust_priv.h"
#include "sd/structs.h"

sd_status_e decode_trame(struct sd_trame*, const uint8_t*);
sd_status_e send_trame(struct sd_handle_st*, const struct sd_trame*);
sd_status_e receive_trame(struct sd_handle_st*, struct sd_trame*);
sd_status_e exchange_trame(struct sd_handle_st*, struct sd_trame*, const struct sd_trame*);
sd_status_e wait_for_trame(struct sd_handle_st*, struct sd_trame*, uint16_t, uint16_t);


#if SD_DEV_MODE
const char* get_system_name(uint8_t);
const char* get_subsystem_name(uint8_t);
const char* get_reset_reason(uint8_t);
#else
#define get_system_name(x) ""
#define get_subsystem_name(x) ""
#define get_reset_reason(x) ""
#endif

#define START_OF_FRAME 0xFE

#define MAX_TRAME_SIZE 258
#define HEADER_SIZE 4
#define CRC_SIZE 1

/*----------------------------------------------*/
/*                    MACRO                     */
/*----------------------------------------------*/
#define EXCHANGE_TRAME(handle, ask, reply, trame_ack_id, timeout)               \
{                                                                               \
    if (send_trame((handle), (ask)) != SD_E_OK)                                 \
    {                                                                           \
        debug("%s: send failed\n", __FUNCTION__);                               \
        return SD_E_KO;                                                         \
    }                                                                           \
                                                                                \
    if (wait_for_trame((handle), (reply), (trame_ack_id), (timeout)) != SD_E_OK)\
    {                                                                           \
        debug("%s: receive failed\n", __FUNCTION__);                            \
        return SD_E_KO;                                                         \
    }                                                                           \
}


#endif //STARDUST_PROTOCOL_H
