//
// Created by madahin on 25/10/23.
//
#include <stdlib.h>
#include "stardust.h"
#include "protocol.h"

#include <stdio.h>

#define START_OF_FRAME 0xFE

void sd_set_driver(struct sd_handle_st* handle, sd_serial_open_fn open, sd_serial_read_fn read, sd_serial_write_fn write, sd_serial_available_fn available, sd_serial_close_fn close, sd_sleep_fn sleep, sd_event_handler_fn event_handler)
{
    if (handle == NULL) return;

    handle->_driver.open          = open;
    handle->_driver.read          = read;
    handle->_driver.write         = write;
    handle->_driver.available     = available;
    handle->_driver.close         = close;
    handle->_driver.sleep         = sleep;
    handle->_driver.event_handler = event_handler;

    handle->_driver_initialized = false;
}

sd_status_e sd_initialize(struct sd_handle_st* handle, void* args)
{
    sd_status_e status = SD_E_KO;

    if (handle == NULL)
    {
        goto exit;
    }

    handle->_driver_initialized = false;

    if (handle->_driver.open(handle, args) != SD_E_OK)
    {
        goto exit;
    }

    handle->_driver_initialized = true;

    sd_soft_reset(handle);

    // At startup, if the device just powered-up, it will send a
    // byte indicating why it lost it's power (usually 0x01 - external)
    handle->_driver.sleep(20);
    if (handle->_driver.available(handle))
    {
        uint8_t buf = 0;
        handle->_driver.read(handle, &buf, 1, &handle->status);
        debug("%s: Last power down reason -> %s (0x%" PRIX8 ")\n", __FUNCTION__, get_reset_reason(buf), buf);

        // Si on a encore des choses a lire ?
        while (handle->_driver.available(handle))
        {
            handle->_driver.sleep(10);
            handle->_driver.read(handle, &buf, 1, &handle->status);
        }
    }

    const int MAX_TRY = 3;
    for (int i=0; (i < MAX_TRY) && (status != SD_E_OK); ++i)
    {
        if (i > 0)
        {
            // On a peut etre recu de la merde, on flush
            while (handle->_driver.available(handle))
            {
                uint8_t buf = 0;
                handle->_driver.read(handle, &buf, 1, &handle->status);
            }
            debug("%s: Trying to connect %d/%d\n", __FUNCTION__, i + 1, MAX_TRY);
        }
        status = sd_ping(handle, &(handle->capabilities));
    }

    if (status == SD_E_OK)
    {
        struct sd_version_st version;
        CheckOk(sd_version(handle, &version), "Unable to retrieve version\n");

        handle->_isV3 = ((version.product_id == 1) || (version.product_id == 2)) ? true : false;
#if SD_DEV_MODE
        debug("Transport revision   : %" PRIu8 "\n", version.transport_rev);
        debug("Product id           : %" PRIu8 "\n", version.product_id);
        debug("major release        : %" PRIu8 "\n", version.major_rel);
        debug("minor release        : %" PRIu8 "\n", version.minor_rel);
        debug("maintenance release  : %" PRIu8 "\n", version.maintenance_rel);
        debug("revision number      : %" PRIu32 "\n", version.revision_num);
        debug("bootloader buildtype : %" PRIu8 "\n", version.bootloader_buildtype);
#endif
    }

exit:
    if ((status != SD_E_OK) && (handle != NULL))
    {
        handle->_driver.close(handle);
    }

    return status;
}

sd_status_e sd_clean(struct sd_handle_st* handle)
{
    if (handle == NULL)
    {
        return SD_E_KO;
    }

    if (handle->_driver.close(handle) != SD_E_OK)
    {
        return SD_E_KO;
    }

    handle->_driver_initialized = false;

    return SD_E_OK;
}

sd_status_e sd_sleep(struct sd_handle_st* handle, const uint32_t ms)
{
    SD_CHECK_DRIVER(handle);

    for (uint32_t i=0; i < ms / 10; ++i)
    {
        sd_update(handle);
        handle->_driver.sleep(10);
    }
    return SD_E_OK;
}

sd_status_e sd_start_coordinator(struct sd_handle_st* handle, uint16_t panid, bool factory_reset)
{
    struct sd_nvm_conf_st nv_conf;
    const uint16_t appProfId = 0x0104;
    const uint16_t appDeviceId = 0x0123;
    const uint8_t appDevVer = 0x0;
    const uint8_t appNumInCluster = 2;
    const uint16_t appInClusterList[] = { 0x0000, 0x0006};
    const uint8_t appNumOutCluster = 2;
    const uint16_t appOutClusterList[] = { 0x0000, 0x0006};
    sd_status_e status = SD_E_OK;

    SD_CHECK_DRIVER(handle);
    SD_CHECK_CAPABILITIES(handle->capabilities, SD_MT_CAP_UTIL | SD_MT_CAP_SAPI);

    // Start with a reset to be in a know stat
    CheckOk(sd_soft_reset(handle), "Failed to reset ???\n");

    // Check our configuration
    CheckOk(sd_read_configuration(handle, ZCD_NV_PANID, &nv_conf), "Failed to read conf\n");

    if ((nv_conf.value[0] == 0xFF) && (nv_conf.value[1] == 0xFF))
    {
        // No config found, we will need to do some initializing
        factory_reset = true;
    }

    if (factory_reset)
    {
        debug("Clear device\n");
        CheckOk(sd_set_startup_option(handle, STARTOPT_CLEAR_CONFIG | STARTOPT_CLEAR_STATE), "Failed to reset ???\n");

        // The clear is effective only after a reset
        debug("Reset\n");
        CheckOk(sd_soft_reset(handle), "Failed to reset ???\n");
        {
            uint64_t extPanid;
            if (panid == 0)
            {
                sd_random(handle, &panid);
            }

            debug("write PANID 0x%02" PRIX16 "\n", panid);

            CheckOk(sd_set_panid(handle, panid), "Failed set PANID ???\n");

            extPanid = (uint64_t)panid;
            debug("write PANID 0x%08" PRIX64 "\n", extPanid);
            CheckOk(sd_set_ext_panid(handle, extPanid), "Failed set extPANID ???\n");
        }

        debug("Set device type as coordinator\n");
        CheckOk(sd_set_device_type(handle, DEV_TYPE_COORDINATOR), "Failed to set device type ???\n");

        debug("Set primary channel as 26 : 2480 Mhz\n");
        CheckOk(sd_set_channel(handle, true, CHANNEL_MASK_26), "Failed to set primary channel mask ???\n");
        debug("Set secondary channel as 25 : 2475 Mhz\n");
        CheckOk(sd_set_channel(handle, false, CHANNEL_MASK_25), "%s: Failed to set secondary channel mask ???\n",
                __FUNCTION__);

        {
            sd_prefcfgkey_t key;
            memset(key, 0xAA, sizeof(sd_prefcfgkey_t));
            debug("Set preconfigured key to : ");
            for (uint8_t i = 0; i < sizeof(sd_prefcfgkey_t); ++i)
            {
                debug("%02" PRIX8 " ", key[i]);
            }
            debug("\n");
            CheckOk(sd_set_prec_cfgkey(handle, key), "%s: Failed to set prec key ???\n", __FUNCTION__);
            debug("Enabling prec key");
            CheckOk(sd_enable_prec_cfgkey(handle, 1), "%s: Failed to enable prec key ???\n", __FUNCTION__);
        }

        /*{
            const sd_tclinkkey_t key = {
                0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
                0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA,
                0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
            };
            debug("Setting trust center key\n");
            CheckOk(sd_write_tc_link_key(handle, key), "%s: Failed to set trust center key ???\n", __FUNCTION__);
        }*/

        debug("Set security level to 0\n");
        CheckOk(sd_set_security_level(handle, 0), "Failed to set security level ???\n");

        debug("Set trust center to allow join\n");
        CheckOk(sd_trust_center_allow_join(handle, true), "Failed to set trust center policy ???\n");

        {
            uint8_t power = 10;
            debug("Set TX power to %" PRIu8 " dbm\n", power);
            CheckOk(sd_set_tx_power(handle, &power), "Failed to set transmission power ???\n");
            debug("Actual TX power: %" PRIu8 " dbm\n", power);
        }

        {
            debug("Activate device direct callback \n");
            CheckOk(sd_activate_direct_callback(handle, 1), "Failed to set device type ???\n");
        }

        //CheckOk(sd_register_endpoint(handle, 1, appProfId, appDeviceId, appDevVer, AF_ENDPOINT_LATENCY_NO_LATENCY, appNumInCluster, appInClusterList, appNumOutCluster, appOutClusterList), "%s: Unable to register endpoint\n", __FUNCTION__);
        //debug("Start commissioning\n");
        //CheckOk(sd_commissioning(handle, COMMISSIONING_MODE_NET_FORMATION), "Unable to start commissioning\n");

    }

    debug("Desactivate key exchange\n");
    CheckOk(sd_set_require_key_exchange(handle, false), "failed to set key exchange requirement\n");



    //debug("Disable join permission\n");
    debug("Enable ZDO callback\n");
    CheckOk(sd_set_ZDO_callback(handle, 0, NULL), "failed to set ZDO callback\n");

    // We wait for the commissioning to end
/*    {
        struct sd_trame reply;

        if (wait_for_trame(handle, &reply, APP_CNF_BDB_COMMISSIONING_NOTIFICATION, 10000) != SD_E_OK)
        {
            debug("%s: commissioning ending receive failed\n", __FUNCTION__);
            return SD_E_KO;
        }

        sd_evt_commissioning(handle, &reply);

        if ((reply.data[1] == BDB_COMMISSIONING_FORMATION) && (reply.data[1] == BDB_COMMISSIONING_SUCCESS))
        {
            CheckOk(sd_commissioning(handle, COMMISSIONING_MODE_NET_STEERING), "Unable to start commissioning\n");
        }
    }*/

    //sd_startup_from_app(handle, 1);
    sd_start_request(handle);

    sd_sleep(handle, 5000);
    CheckOk(sd_set_join_permission(handle, 0xFFFC, 0xFF), "%s: failed to disable join permission\n", __FUNCTION__);
    //CheckOk(sd_allow_join(handle, 0x0F, 0xFFFF, 0xFF, 0), "%s: failed to disable join permission v2\n", __FUNCTION__);
    CheckOk(sd_allow_join(handle, 0x02, 0, 0xFF, 0), "%s: failed to disable join permission v2\n", __FUNCTION__);

    sd_allow_bind(handle, 65);

    //CheckOk(sd_register_endpoint(handle, 1, appProfId, appDeviceId, appDevVer, AF_ENDPOINT_LATENCY_NO_LATENCY, appNumInCluster, appInClusterList, appNumOutCluster, appOutClusterList), "%s: Unable to register endpoint\n", __FUNCTION__);

    //debug("Startup\n");
    //sd_startup_from_app(handle, 1);

    //{
    //    struct sd_device_info_st info;
    //    CheckOk(sd_device_info(handle, &info), "%s: failed to get device info\n", __FUNCTION__);
//
    //    CheckOk(sd_allow_join(handle, 0x0F, info.shortAddr, 0xFF, 0), "%s: failed to disable join permission v3\n", __FUNCTION__);
    //}

    //debug("Scanning for network\n");
    //CheckOk(sd_discover_networks(handle, panid, CHANNEL_MASK_26, 4), "failed to scan\n");

exit:
    return status;
}

sd_status_e sd_start_end_device(struct sd_handle_st* handle, uint16_t panid, bool factory_reset)
{
    struct sd_nvm_conf_st nv_conf;
    const uint16_t appProfId = 0x0104;
    const uint16_t appDeviceId = 0x0123;
    const uint8_t appDevVer = 0x0;
    const uint8_t appNumInCluster = 2;
    const uint16_t appInClusterList[] = { 0x0000, 0x0006};
    const uint8_t appNumOutCluster = 2;
    const uint16_t appOutClusterList[] = { 0x0000, 0x0006};
    sd_status_e status = SD_E_OK;

    SD_CHECK_DRIVER(handle);
    SD_CHECK_CAPABILITIES(handle->capabilities, SD_MT_CAP_UTIL | SD_MT_CAP_SAPI);

    // Start with a reset to be in a know stat
    CheckOk(sd_soft_reset(handle), "Failed to reset ???\n");

    // Check our configuration
    CheckOk(sd_read_configuration(handle, ZCD_NV_PANID, &nv_conf), "Failed to read conf\n");

    if ((nv_conf.value[0] == 0xFF) && (nv_conf.value[1] == 0xFF))
    {
        // No config found, we will need to do some initializing
        factory_reset = true;
    }

    if (factory_reset)
    {
        debug("Clear device\n");
        CheckOk(sd_set_startup_option(handle, STARTOPT_CLEAR_CONFIG | STARTOPT_CLEAR_STATE), "Failed to reset ???\n");

        // The clear is effective only after a reset
        debug("Reset\n");
        CheckOk(sd_soft_reset(handle), "Failed to reset ???\n");
        {
            uint64_t extPanid;
            if (panid == 0)
            {
                sd_random(handle, &panid);
            }

            debug("write PANID 0x%02" PRIX16 "\n", panid);

            CheckOk(sd_set_panid(handle, panid), "Failed set PANID ???\n");

            extPanid = (uint64_t)panid;
            debug("write PANID 0x%08" PRIX64 "\n", extPanid);
            CheckOk(sd_set_ext_panid(handle, extPanid), "Failed set extPANID ???\n");
        }

        debug("Set device type as router\n");
        CheckOk(sd_set_device_type(handle, DEV_TYPE_END_DEVICE), "Failed to set device type ???\n");

        debug("Set primary channel as 26 : 2480 Mhz\n");
        CheckOk(sd_set_channel(handle, true, CHANNEL_MASK_26), "Failed to set primary channel mask ???\n");
        debug("Set secondary channel as 25 : 2475 Mhz\n");
        CheckOk(sd_set_channel(handle, false, CHANNEL_MASK_25), "%s: Failed to set secondary channel mask ???\n", __FUNCTION__);

        {
            sd_prefcfgkey_t key;
            memset(key, 0xAA, sizeof(sd_prefcfgkey_t));
            debug("Set preconfigured key to : ");
            for (uint8_t i = 0; i < sizeof(sd_prefcfgkey_t); ++i)
            {
                debug("%02" PRIX8 " ", key[i]);
            }
            debug("\n");
            CheckOk(sd_set_prec_cfgkey(handle, key), "%s: Failed to set prec key ???\n", __FUNCTION__);
            debug("Enabling prec key");
            CheckOk(sd_enable_prec_cfgkey(handle, 1), "%s: Failed to enable prec key ???\n", __FUNCTION__);
        }

        debug("Set security level to 0\n");
        CheckOk(sd_set_security_level(handle, 0), "Failed to set security level ???\n");

        debug("Set trust center to allow join\n");
        CheckOk(sd_trust_center_allow_join(handle, true), "Failed to set trust center policy ???\n");

        {
            uint8_t power = 10;
            debug("Set TX power to %" PRIu8 " dbm\n", power);
            CheckOk(sd_set_tx_power(handle, &power), "Failed to set transmission power ???\n");
            debug("Actual TX power: %d dbm\n", power);
        }

        {
            debug("Activate device direct callback \n");
            struct sd_nvm_conf_st nv_callback;
            nv_callback.length = 1;
            nv_callback.value[0] = 1;
            CheckOk(sd_write_configuration(handle, ZCD_NV_ZDO_DIRECT_CB, &nv_callback), "Failed to set device type ???\n");
        }

        //CheckOk(sd_register_endpoint(handle, 1, appProfId, appDeviceId, appDevVer, AF_ENDPOINT_LATENCY_NO_LATENCY, appNumInCluster, appInClusterList, appNumOutCluster, appOutClusterList), "%s: Unable to register endpoint\n", __FUNCTION__);
        //debug("Start commissioning\n");
        //CheckOk(sd_commissioning(handle, COMMISSIONING_MODE_NET_FORMATION), "Unable to start commissioning\n");



    }

    debug("Desactivate key exchange\n");
    CheckOk(sd_set_require_key_exchange(handle, false), "failed to set key exchange requirement\n");



    //debug("Disable join permission\n");


    debug("Enable ZDO callback\n");
    CheckOk(sd_set_ZDO_callback(handle, 0, NULL), "failed to set ZDO callback\n");
    sd_sleep(handle, 1000);

    // We wait for the commissioning to end
    //{
    //    struct sd_trame reply;
//
    //    if (wait_for_trame(handle, &reply, APP_CNF_BDB_COMMISSIONING_NOTIFICATION, 65535) != SD_E_OK)
    //    {
    //        debug("%s: commissioning ending receive failed\n", __FUNCTION__);
    //        return SD_E_KO;
    //    }
//
    //    sd_evt_commissioning(handle, &reply);
//
    //    if ((reply.data[1] == BDB_COMMISSIONING_FORMATION) && (reply.data[1] == BDB_COMMISSIONING_SUCCESS))
    //    {
    //        CheckOk(sd_commissioning(handle, COMMISSIONING_MODE_NET_STEERING), "Unable to start commissioning\n");
    //    }
    //}
//
    struct sd_device_info_st di;
    sd_device_info(handle, &di);

    // debug("permit joining request\n");
    // CheckOk(sd_set_join_permission(handle, 0, 0xFF), "%s: failed to disable join permission\n", __FUNCTION__);


    //debug("Scanning for network\n");
    //CheckOk(sd_discover_networks(handle, panid, CHANNEL_MASK_26, 50), "failed to scan\n");

    //sd_commissioning(handle, COMMISSIONING_MODE_NET_FORMATION);

    /*debug("Scanning for network\n");
    CheckOk(sd_discover_networks2(handle, CHANNEL_MASK_26 | CHANNEL_MASK_25, 10), "failed to scan\n");

    {
        struct sd_trame reply;
        if (wait_for_trame(handle, &reply, ZDO_BEACON_NOTIFY_IND, UINT16_MAX) != SD_E_OK)
        {
            debug("%s: discovery ending receive failed\n", __FUNCTION__);
            return SD_E_KO;
        }

        sd_evt_zdo_beacon_notify(handle, &reply);
        {
            uint32_t index_data_reply = 0;
            uint8_t beaconCount = SD_READ_NEXT_1_BYTE(reply.data, index_data_reply);
            debug("Discovered %d networks\n", beaconCount);
            if (beaconCount > 0)
            {
                uint16_t source_addr = SD_READ_NEXT_2_BYTE(reply.data, index_data_reply);
                uint16_t panid2 = SD_READ_NEXT_2_BYTE(reply.data, index_data_reply);
                uint8_t logical_chanel = SD_READ_NEXT_1_BYTE(reply.data, index_data_reply);
                uint8_t permit_join = SD_READ_NEXT_1_BYTE(reply.data, index_data_reply);
                uint8_t router_capacity = SD_READ_NEXT_1_BYTE(reply.data, index_data_reply);
                uint8_t device_capacity = SD_READ_NEXT_1_BYTE(reply.data, index_data_reply);
                uint8_t protocol_version = SD_READ_NEXT_1_BYTE(reply.data, index_data_reply);
                uint8_t stack_profile = SD_READ_NEXT_1_BYTE(reply.data, index_data_reply);
                uint8_t lqi = SD_READ_NEXT_1_BYTE(reply.data, index_data_reply);
                uint8_t depth = SD_READ_NEXT_1_BYTE(reply.data, index_data_reply);
                uint8_t update_id = SD_READ_NEXT_1_BYTE(reply.data, index_data_reply);
                uint64_t extPanID = SD_READ_NEXT_8_BYTE(reply.data, index_data_reply);

                wait_for_trame(handle, &reply, ZDO_NWK_DISCOVERY_CNF, UINT16_MAX);

                //debug("Startup\n");
                //sd_startup_from_app(handle, 1);

                //sd_sleep(handle, 5000);

                debug("sending bind request to network %08" PRIX64 " srcAddr = %" PRIX16 " panid2 = %" PRIX16 "\n",
                      extPanID, source_addr, panid2);
                //CheckOk(sd_bind_request(handle, source_addr, di.IEEEAddr, 1, 0, 0x03, extPanID, 1),
                //        "failed to bind\n");
                sd_bind(handle, true, 1, 0);
            } else
            {
                debug("%s: No network found failed\n", __FUNCTION__);
                return SD_E_KO;
            }
        }
    }
*/
    sd_sleep(handle, 1000);
    debug("Request start\n");
    sd_start_request(handle);

    CheckOk(sd_register_endpoint(handle, 1, appProfId, appDeviceId, appDevVer, AF_ENDPOINT_LATENCY_NO_LATENCY, appNumInCluster, appInClusterList, appNumOutCluster, appOutClusterList), "%s: Unable to register endpoint\n", __FUNCTION__);

    if (factory_reset)
    {
        //sd_bind(handle, true, 1, 0);

        for (uint16_t n = 0; n < 500; ++n)
        {
            sd_update(handle);
            sd_sleep(handle, 10);
        }
    }


    debug("Startup\n");
    sd_startup_from_app(handle, 1);

    if (!factory_reset)
    {
        debug("Try to join last network\n");
        CheckOk(sd_try_rejoin_previous_nwk(handle), "Unable to join network. No network configured/available or not an end device\n");
    }


    //debug("Startup\n");
    //sd_startup_from_app(handle, 1);
    //debug("aaaa\n");

    //sd_sleep(handle, 2000);
    //'d_commissioning(handle, COMMISSIONING_MODE_NET_FORMATION);
    //'ebug("bbbb\n");



    //CheckOk(sd_request_IEEE_addr(handle, SHORT_ADDRESS_COORDINATOR, 0x01, 0x00), "failed request IEEE address\n");

    //debug("Request IEEE address of coordinator\n");
    //CheckOk(sd_request_IEEE_addr(handle, SHORT_ADDRESS_COORDINATOR, 0x01, 0x00), "failed request IEEE address\n");

exit:
    debug("%s status = %d", __FUNCTION__ , status);
    return status;
}

sd_status_e sd_update_(struct sd_handle_st* handle, const struct sd_trame* callback)
{
    SD_CHECK_DRIVER(handle);

    // bool eat_event = false;

    const uint16_t command_id = GET_COMMAND_ID(*callback);
    debug("Received trame %s (0x%04X)\n", GET_CMD_NAME(callback), command_id);
    switch (command_id)
    {
        case APP_CNF_BDB_COMMISSIONING_NOTIFICATION:
        {
            sd_evt_commissioning(handle, callback);
            break;
        }
        case SYS_RESET_REQ_ACK:
        {
            debug("Reason -> %s\n", get_reset_reason(callback->data[0]));
            break;
        }
        case ZB_BIND_CONFIRM:
        {
            sd_evt_bind_confirm(handle, callback);
            break;
        }
        case ZB_ALLOW_BIND_CONFIRM:
        {
            sd_evt_allow_bind_confirm(handle, callback);
            break;
        }
        case AF_INCOMING_MSG:
        {
            sd_evt_af_incoming_msg(handle, callback);
            break;
        }
        case ZDO_MGMT_BIND_RSP:
        {
            sd_evt_mgm_bind(handle, callback);
            break;
        }
        case ZDO_STATE_CHANGE_IND:
        {
            sd_evt_zdo_state_change(handle, callback);
            break;
        }
        case ZDO_BEACON_NOTIFY_IND:
        {
            sd_evt_zdo_beacon_notify(handle, callback);
            break;
        }
        case ZDO_CONCENTRATOR_IND_CB:
        {
            sd_evt_zdo_concentrator_ind(handle, callback);
            break;
        }
        case ZDO_END_DEVICE_ANNCE_IND:
        {
            sd_evt_zdo_dev_annce_ind(handle, callback);
            break;
        }
        case ZDO_SRC_RTG_IND:
        {
            sd_evt_zdo_src_rtg_ind(handle, callback);
            break;
        }
        case ZDO_TC_DEV_IND:
        {
            sd_evt_zdo_dev_ind(handle, callback);
            break;
        }
        case ZDO_NWK_DISCOVERY_CNF:
        {
            sd_evt_zdo_nwk_discovery(handle, callback);
            break;
        }
        case ZDO_MGMT_PERMIT_JOIN_RSP:
        {
            break;
        }
        case ZDO_MSG_CB_INCOMING:
        {
            sd_evt_zdo_msg_cb_incoming(handle, callback);
            break;
        }
        default:
        {
            debug("unknown message :");
            for (int i = 0; i < callback->length; ++i)
            {
                debug(" %02" PRIX8, callback->data[i]);
            }
            debug("\n");
            break;
        }
    }

    // if (!eat_event)
    {
        handle->_driver.event_handler(callback, command_id);
    }

    return SD_E_OK;
}

sd_status_e sd_update(struct sd_handle_st* handle)
{
    struct sd_trame callback;

    SD_CHECK_DRIVER(handle);

    if (handle->_driver.available(handle))
    {
        if (receive_trame(handle, &callback) == SD_E_OK)
        {
            return sd_update_(handle, &callback);
        }
    }

    return SD_E_OK;
}
