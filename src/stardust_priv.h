//
// Created by madahin on 29/10/23.
//

#ifndef STARDUST_STARDUST_PRIV_H
#define STARDUST_STARDUST_PRIV_H

#include <stdio.h>
#include <inttypes.h>

#if SD_DEV_MODE
#define debug(...) printf(__VA_ARGS__)
#else
#define debug(...)
#endif

#define SD_CHECK_DRIVER(handle)                             \
{                                                           \
    if ((handle)->_driver_initialized == false)              \
    {                                                       \
        debug("%s: driver uninitialized\n", __FUNCTION__);  \
        return SD_E_KO;                                     \
    }                                                       \
}

#define SD_CHECK_CAPABILITIES(capabilities, capabilities_needed)                   \
{                                                                                  \
    if (((capabilities) & (capabilities_needed)) != (capabilities_needed))         \
    {                                                                              \
        debug("%s: devices doesn't have all needed capabilities\n", __FUNCTION__); \
        return SD_E_KO;                                                            \
    }                                                                              \
}

#define SD_CHECK_MANDATORY_ARGUMENT(arg)                    \
{                                                           \
    if ((arg) == NULL)                                      \
    {                                                       \
        debug("%s: argument is mandatory\n", __FUNCTION__); \
        return SD_E_KO;                                     \
    }                                                       \
}

#define CheckOk(X, ...) if ((X) != SD_E_OK) { debug(__VA_ARGS__); status = SD_E_KO; goto exit;}

#define SYS_RESET_REQ_TYPE_HARD 0
#define SYS_RESET_REQ_TYPE_SOFT 1

#define SD_MAKE_DEBUG_CMD(cmd) { (#cmd), (cmd) }

struct sd_debug_id_st {
    const char* cmdName;
    uint16_t cmdId;
};

sd_status_e sd_update_(struct sd_handle_st*, const struct sd_trame*);

#endif //STARDUST_STARDUST_PRIV_H
