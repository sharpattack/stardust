//
// Created by madahin on 05/11/23.
//

#include "protocol.h"

#include "sd/commands.h"
#include "sd/config.h"
#include "stardust_priv.h"

#if SD_DEV_MODE
struct sd_debug_id_st cmd_name[] = {
    SD_MAKE_DEBUG_CMD(SYS_RESET_REQ                              ),
    SD_MAKE_DEBUG_CMD(SYS_RESET_REQ_ACK                          ),
    SD_MAKE_DEBUG_CMD(SYS_PING                                   ),
    SD_MAKE_DEBUG_CMD(SYS_PING_ACK                               ),
    SD_MAKE_DEBUG_CMD(SYS_VERSION                                ),
    SD_MAKE_DEBUG_CMD(SYS_VERSION_ACK                            ),
    SD_MAKE_DEBUG_CMD(SYS_OSAL_NV_WRITE                          ),
    SD_MAKE_DEBUG_CMD(SYS_OSAL_NV_WRITE_ACK                      ),
    SD_MAKE_DEBUG_CMD(SYS_RANDOM                                 ),
    SD_MAKE_DEBUG_CMD(SYS_RANDOM_ACK                             ),
    SD_MAKE_DEBUG_CMD(SYS_SET_TX_POWER                           ),
    SD_MAKE_DEBUG_CMD(SYS_SET_TX_POWER_ACK                       ),
    SD_MAKE_DEBUG_CMD(UTIL_GET_DEVICE_INFO                       ),
    SD_MAKE_DEBUG_CMD(UTIL_GET_DEVICE_INFO_ACK                   ),
    SD_MAKE_DEBUG_CMD(ZB_ALLOW_BIND                              ),
    SD_MAKE_DEBUG_CMD(ZB_ALLOW_BIND_ACK                          ),
    SD_MAKE_DEBUG_CMD(ZB_SEND_DATA_REQUEST                       ),
    SD_MAKE_DEBUG_CMD(ZB_SEND_DATA_REQUEST_ACK                   ),
    SD_MAKE_DEBUG_CMD(ZB_START_REQUEST                           ),
    SD_MAKE_DEBUG_CMD(ZB_START_REQUEST_ACK                       ),
    SD_MAKE_DEBUG_CMD(ZB_BIND_DEVICE                             ),
    SD_MAKE_DEBUG_CMD(ZB_BIND_DEVICE_ACK                         ),
    SD_MAKE_DEBUG_CMD(UTIL_SET_SECLEVEL                          ),
    SD_MAKE_DEBUG_CMD(UTIL_SET_SECLEVEL_ACK                      ),
    SD_MAKE_DEBUG_CMD(ZB_READ_CONF                               ),
    SD_MAKE_DEBUG_CMD(ZB_READ_CONF_ACK                           ),
    SD_MAKE_DEBUG_CMD(ZB_WRITE_CONF                              ),
    SD_MAKE_DEBUG_CMD(ZB_WRITE_CONF_ACK                          ),
    SD_MAKE_DEBUG_CMD(ZB_PERMIT_JOINING_REQUEST                  ),
    SD_MAKE_DEBUG_CMD(ZB_PERMIT_JOINING_REQUEST_ACK              ),
    SD_MAKE_DEBUG_CMD(ZB_BIND_CONFIRM                            ),
    SD_MAKE_DEBUG_CMD(ZB_ALLOW_BIND_CONFIRM                      ),
    SD_MAKE_DEBUG_CMD(AF_REGISTER                                ),
    SD_MAKE_DEBUG_CMD(AF_REGISTER_ACK                            ),
    SD_MAKE_DEBUG_CMD(AF_DATA_REQUEST                            ),
    SD_MAKE_DEBUG_CMD(AF_DATA_REQUEST_ACK                        ),
    SD_MAKE_DEBUG_CMD(AF_INCOMING_MSG                            ),
    SD_MAKE_DEBUG_CMD(ZDO_IEEE_ADDR_REQ                          ),
    SD_MAKE_DEBUG_CMD(ZDO_IEEE_ADDR_REQ_ACK                      ),
    SD_MAKE_DEBUG_CMD(ZDO_BIND_REQ                               ),
    SD_MAKE_DEBUG_CMD(ZDO_BIND_REQ_ACK                           ),
    SD_MAKE_DEBUG_CMD(ZDO_NWK_DISCOVERY_REQ                      ),
    SD_MAKE_DEBUG_CMD(ZDO_NWK_DISCOVERY_REQ_ACK                  ),
    SD_MAKE_DEBUG_CMD(ZDO_MGMT_NWK_DISC_REQ                      ),
    SD_MAKE_DEBUG_CMD(ZDO_MGMT_NWK_DISC_REQ_ACK                  ),
    SD_MAKE_DEBUG_CMD(ZDO_MGMT_BIND_REQ                          ),
    SD_MAKE_DEBUG_CMD(ZDO_MGMT_BIND_REQ_ACK                      ),
    SD_MAKE_DEBUG_CMD(ZDO_MGMT_PERMIT_JOIN_REQ                   ),
    SD_MAKE_DEBUG_CMD(ZDO_MGMT_PERMIT_JOIN_REQ_ACK               ),
    SD_MAKE_DEBUG_CMD(ZDO_MSG_CB_REGISTER_REQ                    ),
    SD_MAKE_DEBUG_CMD(ZDO_MSG_CB_REGISTER_REQ_ACK                ),
    SD_MAKE_DEBUG_CMD(ZDO_STARTUP_FROM_APP                       ),
    SD_MAKE_DEBUG_CMD(ZDO_STARTUP_FROM_APP_ACK                   ),
    SD_MAKE_DEBUG_CMD(ZDO_STARTUP_FROM_APP_EX                    ),
    SD_MAKE_DEBUG_CMD(ZDO_IEEE_ADDR_RSP                          ),
    SD_MAKE_DEBUG_CMD(ZDO_BIND_RSP                               ),
    SD_MAKE_DEBUG_CMD(ZDO_MGMT_NWK_DISC_RSP                      ),
    SD_MAKE_DEBUG_CMD(ZDO_MGMT_BIND_RSP                          ),
    SD_MAKE_DEBUG_CMD(ZDO_MGMT_PERMIT_JOIN_RSP                   ),
    SD_MAKE_DEBUG_CMD(ZDO_STATE_CHANGE_IND                       ),
    SD_MAKE_DEBUG_CMD(ZDO_END_DEVICE_ANNCE_IND                   ),
    SD_MAKE_DEBUG_CMD(ZDO_SRC_RTG_IND                            ),
    SD_MAKE_DEBUG_CMD(ZDO_BEACON_NOTIFY_IND                      ),
    SD_MAKE_DEBUG_CMD(ZDO_NWK_DISCOVERY_CNF                      ),
    SD_MAKE_DEBUG_CMD(ZDO_CONCENTRATOR_IND_CB                    ),
    SD_MAKE_DEBUG_CMD(ZDO_TC_DEV_IND                             ),
    SD_MAKE_DEBUG_CMD(ZDO_MSG_CB_INCOMING                        ),
    SD_MAKE_DEBUG_CMD(APP_CNF_SET_ALLOWREJOIN_TC_POLICY          ),
    SD_MAKE_DEBUG_CMD(APP_CNF_SET_ALLOWREJOIN_TC_POLICY_ACK      ),
    SD_MAKE_DEBUG_CMD(APP_CNF_BDB_START_COMMISSIONING            ),
    SD_MAKE_DEBUG_CMD(APP_CNF_BDB_START_COMMISSIONING_ACK        ),
    SD_MAKE_DEBUG_CMD(APP_CNF_BDB_SET_CHANNEL                    ),
    SD_MAKE_DEBUG_CMD(APP_CNF_BDB_SET_CHANNEL_ACK                ),
    SD_MAKE_DEBUG_CMD(APP_CNF_BDB_SET_TC_REQUIRE_KEY_EXCHANGE    ),
    SD_MAKE_DEBUG_CMD(APP_CNF_BDB_SET_TC_REQUIRE_KEY_EXCHANGE_ACK),
    SD_MAKE_DEBUG_CMD(APP_CNF_BDB_ZED_ATTEMPT_RECOVER_NWK        ),
    SD_MAKE_DEBUG_CMD(APP_CNF_BDB_ZED_ATTEMPT_RECOVER_NWK_ACK    ),
    SD_MAKE_DEBUG_CMD(APP_CNF_BDB_COMMISSIONING_NOTIFICATION     ),
    {NULL, 0}
};

struct sd_debug_id_st commissioning_name[] = {
    SD_MAKE_DEBUG_CMD(BDB_COMMISSIONING_INITIALIZATION ),
    SD_MAKE_DEBUG_CMD(BDB_COMMISSIONING_NWK_STEERING   ),
    SD_MAKE_DEBUG_CMD(BDB_COMMISSIONING_FORMATION      ),
    SD_MAKE_DEBUG_CMD(BDB_COMMISSIONING_FINDING_BINDING),
    SD_MAKE_DEBUG_CMD(BDB_COMMISSIONING_TOUCHLINK      ),
    SD_MAKE_DEBUG_CMD(BDB_COMMISSIONING_PARENT_LOST    ),
    {NULL, 0}
};

struct sd_debug_id_st commissioning_mode_name[] = {
    SD_MAKE_DEBUG_CMD(COMMISSIONING_MODE_INITIALIZATION),
    SD_MAKE_DEBUG_CMD(COMMISSIONING_MODE_TOUCHLINK     ),
    SD_MAKE_DEBUG_CMD(COMMISSIONING_MODE_NET_STEERING  ),
    SD_MAKE_DEBUG_CMD(COMMISSIONING_MODE_NET_FORMATION ),
    SD_MAKE_DEBUG_CMD(COMMISSIONING_MODE_FIND_AND_BIND ),
    {NULL, 0}
};

struct sd_debug_id_st commissioning_status[] = {
    SD_MAKE_DEBUG_CMD(BDB_COMMISSIONING_SUCCESS                      ),
    SD_MAKE_DEBUG_CMD(BDB_COMMISSIONING_IN_PROGRESS                  ),
    SD_MAKE_DEBUG_CMD(BDB_COMMISSIONING_NO_NETWORK                   ),
    SD_MAKE_DEBUG_CMD(BDB_COMMISSIONING_TL_TARGET_FAILURE            ),
    SD_MAKE_DEBUG_CMD(BDB_COMMISSIONING_TL_NOT_AA_CAPABLE            ),
    SD_MAKE_DEBUG_CMD(BDB_COMMISSIONING_TL_NO_SCAN_RESPONSE          ),
    SD_MAKE_DEBUG_CMD(BDB_COMMISSIONING_TL_NOT_PERMITTED             ),
    SD_MAKE_DEBUG_CMD(BDB_COMMISSIONING_TCLK_EX_FAILURE              ),
    SD_MAKE_DEBUG_CMD(BDB_COMMISSIONING_FORMATION_FAILURE            ),
    SD_MAKE_DEBUG_CMD(BDB_COMMISSIONING_FB_TARGET_IN_PROGRESS        ),
    SD_MAKE_DEBUG_CMD(BDB_COMMISSIONING_FB_INITIATOR_IN_PROGRESS     ),
    SD_MAKE_DEBUG_CMD(BDB_COMMISSIONING_FB_NO_IDENTIFY_QUERY_RESPONSE),
    SD_MAKE_DEBUG_CMD(BDB_COMMISSIONING_FB_BINDING_TABLE_FULL        ),
    SD_MAKE_DEBUG_CMD(BDB_COMMISSIONING_NETWORK_RESTORED             ),
    SD_MAKE_DEBUG_CMD(BDB_COMMISSIONING_FAILURE                      ),
    {NULL, 0}
};

struct sd_debug_id_st zdo_status[] = {
    SD_MAKE_DEBUG_CMD(ZDO_STATUS_HOLD             ),
    SD_MAKE_DEBUG_CMD(ZDO_STATUS_INIT             ),
    SD_MAKE_DEBUG_CMD(ZDO_STATUS_NWK_DISC         ),
    SD_MAKE_DEBUG_CMD(ZDO_STATUS_NWK_JOINING      ),
    SD_MAKE_DEBUG_CMD(ZDO_STATUS_NWK_REJOINING    ),
    SD_MAKE_DEBUG_CMD(ZDO_STATUS_END_DEVICE_UNAUTH),
    SD_MAKE_DEBUG_CMD(ZDO_STATUS_END_DEVICE       ),
    SD_MAKE_DEBUG_CMD(ZDO_STATUS_ROUTER           ),
    SD_MAKE_DEBUG_CMD(ZDO_STATUS_COORD_STARTING   ),
    SD_MAKE_DEBUG_CMD(ZDO_STATUS_ZB_COORD         ),
    SD_MAKE_DEBUG_CMD(ZDO_STATUS_NWK_ORPHAN       ),
    {NULL, 0}
};

struct sd_debug_id_st nwk_discovery_status[] = {
        SD_MAKE_DEBUG_CMD(ZMAC_SUCCESS          ),
        SD_MAKE_DEBUG_CMD(ZMAC_NO_BEACON        ),
        SD_MAKE_DEBUG_CMD(ZMAC_INVALID_PARAMETER),
        {NULL, 0}
};

#endif

const char* get_name_from_id(const struct sd_debug_id_st*, const uint16_t);

#if SD_DEV_MODE

#endif

#pragma region MT_AF
sd_status_e sd_register_endpoint(struct sd_handle_st* handle, const uint8_t endpoint, const uint16_t appProfId, const uint16_t appDeviceId, const uint8_t appDevVer, const uint8_t latencyReq, const uint8_t appNumInCluster, const uint16_t* appInClusterList, const uint8_t appNumOutCluster, const uint16_t* appOutClusterList)
{
    struct sd_trame ask   = { 0 };
    struct sd_trame reply = { 0 };
    uint8_t buf[0x49] = {0};

    SD_CHECK_DRIVER(handle);
    SD_CHECK_CAPABILITIES(handle->capabilities, SD_MT_CAP_AF);

    SET_COMMAND_ID(ask, AF_REGISTER);

    //uint16_t appProfId = 0x0104;
    //uint16_t appDeviceId = 0x0123;
    //uint8_t appDevVer = 0x89;

    SD_WRITE_NEXT_1_BYTE(buf, ask.length, endpoint);
    SD_WRITE_NEXT_2_BYTE(buf, ask.length, appProfId);
    SD_WRITE_NEXT_2_BYTE(buf, ask.length, appDeviceId);
    SD_WRITE_NEXT_1_BYTE(buf, ask.length, appDevVer);
    SD_WRITE_NEXT_1_BYTE(buf, ask.length, latencyReq);
    SD_WRITE_NEXT_1_BYTE(buf, ask.length, appNumInCluster);
    for (uint8_t n = 0; n < appNumInCluster; ++n)
    {
        SD_WRITE_NEXT_2_BYTE(buf, ask.length, appInClusterList[n]);
    }
    SD_WRITE_NEXT_1_BYTE(buf, ask.length, appNumOutCluster);
    for (uint8_t n = 0; n < appNumInCluster; ++n)
    {
        SD_WRITE_NEXT_2_BYTE(buf, ask.length, appOutClusterList[n]);
    }

    ask.data = buf;

    //buf[0] = endpoint;
    //// AppProfId
    //buf[1] = (appProfId)      & 0xFF;
    //buf[2] = (appProfId >> 4) & 0xFF;
    //// AppDeviceId
    //buf[3] = (appDeviceId)      & 0xFF;
    //buf[4] = (appDeviceId >> 4) & 0xFF;
    //// AddDevVer
    //buf[5] = 0x89;
    //// LatencyReq
    //buf[6] = AF_ENDPOINT_LATENCY_FAST;
    //// Cluster
    //buf[7] = 0x00;
    //buf[8] = 0x00;
    //ask.data = buf;
    //ask.length = sizeof(buf);

    EXCHANGE_TRAME(handle, &ask, &reply, AF_REGISTER_ACK, 100);

    return SD_CHECK_CMD_STATUS(reply);
}

sd_status_e sd_send_data_to_endpoint(struct sd_handle_st* handle, const uint16_t dstAddr, const uint8_t dstEndpoint, const uint8_t srcEndpoint, const uint16_t clusterId, const uint8_t transId, const uint8_t options, const uint8_t radius, const uint8_t len, const uint8_t* data)
{
    struct sd_trame ask   = { 0 };
    struct sd_trame reply = { 0 };
    uint8_t buf[0x8A]     = { 0 };

    SD_CHECK_DRIVER(handle);
    SD_CHECK_CAPABILITIES(handle->capabilities, SD_MT_CAP_AF);
    if (len > 128)
    {
        debug("%s: input buffer too big: %" PRIu8 " > 128\n", __FUNCTION__, len);
        return SD_E_KO;
    }

    SET_COMMAND_ID(ask, AF_DATA_REQUEST);
    SD_WRITE_NEXT_2_BYTE(buf, ask.length, dstAddr);
    SD_WRITE_NEXT_2_BYTE(buf, ask.length, dstEndpoint);
    SD_WRITE_NEXT_1_BYTE(buf, ask.length, srcEndpoint);
    SD_WRITE_NEXT_1_BYTE(buf, ask.length, clusterId);
    SD_WRITE_NEXT_1_BYTE(buf, ask.length, transId);
    SD_WRITE_NEXT_1_BYTE(buf, ask.length, options);
    SD_WRITE_NEXT_1_BYTE(buf, ask.length, radius);
    SD_WRITE_NEXT_1_BYTE(buf, ask.length, len);
    memcpy(buf + ask.length, data, len);
    ask.length += len;

    ask.data = buf;

    EXCHANGE_TRAME(handle, &ask, &reply, AF_DATA_REQUEST_ACK, 5000);

    return SD_CHECK_CMD_STATUS(reply);;
}
#pragma endregion MT_AF

#pragma region MT_APP
#pragma endregion MT_APP

#pragma region MT_DEBUG
#pragma endregion MT_DEBUG

#pragma region MT_MAC
#pragma endregion MT_MAC

#pragma region MT_SAPI
sd_status_e sd_start_request(struct sd_handle_st* handle)
{
    struct sd_trame ask   = { 0 };
    struct sd_trame reply = { 0 };

    SD_CHECK_DRIVER(handle);
    SD_CHECK_CAPABILITIES(handle->capabilities, SD_MT_CAP_SAPI);

    SET_COMMAND_ID(ask, ZB_START_REQUEST);

    EXCHANGE_TRAME(handle, &ask, &reply, ZB_START_REQUEST_ACK, 5000);

    debug("reply :");
    for (int i = 0; i < reply.length; ++i)
    {
        debug(" %" PRIX8, reply.data[i]);
    }
    debug("\n");

    return SD_E_OK;
}

sd_status_e sd_bind(struct sd_handle_st* handle, bool create, uint16_t commandId, uint64_t destination)
{
    struct sd_trame ask   = { 0 };
    struct sd_trame reply = { 0 };
    uint8_t buf[0x0B]     = { 0 };

    SD_CHECK_DRIVER(handle);
    SD_CHECK_CAPABILITIES(handle->capabilities, SD_MT_CAP_SAPI);

    SET_COMMAND_ID(ask, ZB_BIND_DEVICE);

    SD_WRITE_NEXT_1_BYTE(buf, ask.length, (create) ? 1 : 0);
    SD_WRITE_NEXT_2_BYTE(buf, ask.length, commandId);
    SD_WRITE_NEXT_8_BYTE(buf, ask.length, destination);

    ask.data = buf;

    EXCHANGE_TRAME(handle, &ask, &reply, ZB_BIND_DEVICE_ACK, 5000);

    return SD_E_OK;
}

sd_status_e sd_send_data(struct sd_handle_st* handle, const uint16_t destination, const uint16_t commandID, const uint8_t dataHandle, const uint8_t ack, const uint8_t radius, const uint8_t dataLen, const uint8_t* data)
{
    struct sd_trame ask   = { 0 };
    struct sd_trame reply = { 0 };
    uint8_t buf[0x5C]     = { 0 };

    SD_CHECK_DRIVER(handle);
    SD_CHECK_CAPABILITIES(handle->capabilities, SD_MT_CAP_SAPI);

    if (dataLen > 84)
    {
        debug("%s: input buffer too big: %" PRIu8 " > 84\n", __FUNCTION__, dataLen);
        return SD_E_KO;
    }

    SET_COMMAND_ID(ask, ZB_SEND_DATA_REQUEST);

    SD_WRITE_NEXT_2_BYTE(buf, ask.length, destination);
    SD_WRITE_NEXT_2_BYTE(buf, ask.length, commandID);
    SD_WRITE_NEXT_1_BYTE(buf, ask.length, dataHandle);
    SD_WRITE_NEXT_1_BYTE(buf, ask.length, ack);
    SD_WRITE_NEXT_1_BYTE(buf, ask.length, radius);
    SD_WRITE_NEXT_1_BYTE(buf, ask.length, dataLen);
    memcpy(buf + ask.length, data, dataLen);
    ask.length += dataLen;

    ask.data = buf;

    EXCHANGE_TRAME(handle, &ask, &reply, ZB_SEND_DATA_REQUEST_ACK, 5000);

    return SD_E_OK;
}

sd_status_e sd_read_configuration(struct sd_handle_st* handle, const uint8_t confId, struct sd_nvm_conf_st* conf)
{
    struct sd_trame ask   = { 0 };
    struct sd_trame reply = { 0 };

    SD_CHECK_DRIVER(handle);
    SD_CHECK_MANDATORY_ARGUMENT(conf);
    SD_CHECK_CAPABILITIES(handle->capabilities, SD_MT_CAP_SAPI);

    SET_COMMAND_ID(ask, ZB_READ_CONF);
    ask.length = 1;
    ask.data = &confId;

    EXCHANGE_TRAME(handle, &ask, &reply, ZB_READ_CONF_ACK, 100);

    if (reply.data[0] != 0)
    {
        debug("%s: periph failure (%" PRIu8 ")\n", __FUNCTION__, reply.data[0]);
        return SD_E_KO;
    }

    conf->length = reply.data[2];
    memcpy(conf->value, reply.data + 3, conf->length);

    return SD_E_OK;
}

sd_status_e sd_write_configuration(struct sd_handle_st* handle, const uint8_t confId, const struct sd_nvm_conf_st* conf)
{
    struct sd_trame ask   = { 0 };
    struct sd_trame reply = { 0 };
    uint8_t buf[sizeof(struct sd_nvm_conf_st) + 1] = { 0 };

    SD_CHECK_DRIVER(handle);
    SD_CHECK_MANDATORY_ARGUMENT(conf);
    SD_CHECK_CAPABILITIES(handle->capabilities, SD_MT_CAP_SAPI);

    SET_COMMAND_ID(ask, ZB_WRITE_CONF);
    ask.length = 2 + conf->length;

    buf[0] = confId;
    buf[1] = conf->length;
    memcpy(buf + 2, conf->value, conf->length);

    ask.data = buf;

    EXCHANGE_TRAME(handle, &ask, &reply, ZB_WRITE_CONF_ACK, 100);

    return SD_CHECK_CMD_STATUS(reply);
}

sd_status_e sd_set_startup_option(struct sd_handle_st* handle, const uint8_t option)
{
    struct sd_nvm_conf_st input = { 0 };
    SD_WRITE_NEXT_1_BYTE(input.value, input.length, option);
    return sd_write_configuration(handle, ZCD_NV_STARTUP_OPTION, &input);
}
sd_status_e sd_set_ext_panid(struct sd_handle_st* handle, const uint64_t ext_panid)
{
    struct sd_nvm_conf_st input = { 0 };
    SD_WRITE_NEXT_8_BYTE(input.value, input.length, ext_panid);
    return sd_write_configuration(handle, ZCD_NV_EXT_PANID, &input);
}

sd_status_e sd_set_prec_cfgkey(struct sd_handle_st* handle, const sd_prefcfgkey_t key)
{
    struct sd_nvm_conf_st input = { 0 };
    input.length = 16;
    memcpy(input.value, key, input.length);
    return sd_write_configuration(handle, ZCD_NV_PRECFGKEY, &input);
}

sd_status_e sd_enable_prec_cfgkey(struct sd_handle_st* handle, const uint8_t enable)
{
    struct sd_nvm_conf_st input = { 0 };
    SD_WRITE_NEXT_1_BYTE(input.value, input.length, enable);
    return sd_write_configuration(handle, ZCD_NV_PRECFGKEY_ENABLE, &input);
}

sd_status_e sd_set_device_type(struct sd_handle_st* handle, const uint8_t dev_type)
{
    struct sd_nvm_conf_st input = { 0 };
    SD_WRITE_NEXT_1_BYTE(input.value, input.length, dev_type);

    return sd_write_configuration(handle, ZCD_NV_LOGICAL_TYPE, &input);
}

sd_status_e sd_activate_direct_callback(struct sd_handle_st* handle, const uint8_t enable)
{
    struct sd_nvm_conf_st input = { 0 };
    SD_WRITE_NEXT_1_BYTE(input.value, input.length, enable);
    return sd_write_configuration(handle, ZCD_NV_ZDO_DIRECT_CB, &input);
}

sd_status_e sd_set_panid(struct sd_handle_st* handle, const uint16_t panid)
{
    struct sd_nvm_conf_st input = { 0 };
    SD_WRITE_NEXT_2_BYTE(input.value, input.length, panid);
    return sd_write_configuration(handle, ZCD_NV_PANID, &input);
}

sd_status_e sd_set_join_permission(struct sd_handle_st* handle, const uint16_t destination, const uint8_t timeout)
{
    struct sd_trame ask   = { 0 };
    struct sd_trame reply = { 0 };
    uint8_t buf[3] = { 0 };

    SD_CHECK_DRIVER(handle);
    SD_CHECK_CAPABILITIES(handle->capabilities, SD_MT_CAP_SAPI);

    SET_COMMAND_ID(ask, ZB_PERMIT_JOINING_REQUEST);

    SD_WRITE_NEXT_2_BYTE(buf, ask.length, destination);
    SD_WRITE_NEXT_1_BYTE(buf, ask.length, timeout);

    ask.data = buf;

    EXCHANGE_TRAME(handle, &ask, &reply, ZB_PERMIT_JOINING_REQUEST_ACK, 1000);

    if (reply.data[0] == 0)
    {
        if (wait_for_trame(handle, &reply, ZDO_MGMT_PERMIT_JOIN_RSP, 10000) != SD_E_OK)
        {
            debug("%s: receive failed\n", __FUNCTION__);
            return SD_E_KO;
        }

        uint16_t srcAddr = SD_READ_2_BYTE(reply.data, 0);
        uint8_t status = SD_READ_1_BYTE(reply.data, 2);

        debug("%s srcAddr=%" PRIu16 "\n", __FUNCTION__, srcAddr);
        debug("%s status=%" PRIu8 "\n", __FUNCTION__, status);

        return SD_CHECK_CMD_STATUS(reply);
    }

    debug("%s status=%" PRIu8 "\n", __FUNCTION__, reply.data[0]);

    return SD_E_KO;
}

void sd_evt_bind_confirm(struct sd_handle_st* handle, const struct sd_trame* callback)
{
    uint8_t index_data_reply = 0;
    const uint16_t commandId = SD_READ_NEXT_2_BYTE(callback->data, index_data_reply);
    const uint8_t status = SD_READ_NEXT_1_BYTE(callback->data, index_data_reply);

    debug("%s commandId = %" PRIX16 "\n", __FUNCTION__, commandId);
    debug("%s status = %" PRIX8 "\n", __FUNCTION__, status);
}

void sd_evt_allow_bind_confirm(struct sd_handle_st* handle, const struct sd_trame* callback)
{
    uint8_t index_data_reply = 0;
    const uint16_t source = SD_READ_NEXT_2_BYTE(callback->data, index_data_reply);

    debug("%s source = %" PRIX16 "\n", __FUNCTION__, source);
}

#pragma endregion MT_SAPI

#pragma region MT_SYS
static sd_status_e sd_reset(struct sd_handle_st* handle, const uint8_t reset_type)
{
    struct sd_trame ask   = { 0 };
    struct sd_trame reply = { 0 };

    SD_CHECK_DRIVER(handle);

    SET_COMMAND_ID(ask, SYS_RESET_REQ);
    ask.length = 1;
    ask.data = &reset_type;

    EXCHANGE_TRAME(handle, &ask, &reply, SYS_RESET_REQ_ACK, 10000);

    debug("%s: reason -> %s\n", __FUNCTION__, get_reset_reason(reply.data[0]));

    return SD_E_OK;
}

sd_status_e sd_soft_reset(struct sd_handle_st* handle)
{
    return sd_reset(handle, SYS_RESET_REQ_TYPE_SOFT);
}

sd_status_e sd_hard_reset(struct sd_handle_st* handle)
{
    return sd_reset(handle, SYS_RESET_REQ_TYPE_HARD);
}

sd_status_e sd_ping(struct sd_handle_st* handle, uint16_t* capabilities)
{
    struct sd_trame ask   = { 0 };
    struct sd_trame reply = { 0 };

    SD_CHECK_DRIVER(handle);

    SET_COMMAND_ID(ask, SYS_PING);

    ask.length = 0;

    EXCHANGE_TRAME(handle, &ask, &reply, SYS_PING_ACK, 100);

    if (capabilities != NULL)
    {
        *capabilities = SD_READ_2_BYTE(reply.data, 0);
    }

    return SD_E_OK;
}

sd_status_e sd_version(struct sd_handle_st* handle, struct sd_version_st* version)
{
    struct sd_trame ask   = { 0 };
    struct sd_trame reply = { 0 };
    uint8_t index_data_reply = 0;

    SD_CHECK_DRIVER(handle);
    SD_CHECK_MANDATORY_ARGUMENT(version);
    SD_CHECK_CAPABILITIES(handle->capabilities, SD_MT_CAP_SYS);

    SET_COMMAND_ID(ask, SYS_VERSION);
    ask.length = 0;

    EXCHANGE_TRAME(handle, &ask, &reply, SYS_VERSION_ACK, 100);

    version->transport_rev = SD_READ_NEXT_1_BYTE(reply.data, index_data_reply);
    version->product_id = SD_READ_NEXT_1_BYTE(reply.data, index_data_reply);
    version->major_rel = SD_READ_NEXT_1_BYTE(reply.data, index_data_reply);
    version->minor_rel = SD_READ_NEXT_1_BYTE(reply.data, index_data_reply);
    version->maintenance_rel = SD_READ_NEXT_1_BYTE(reply.data, index_data_reply);

    // Z-Stack include revision information
    if (reply.length == 5)
    {
        debug("%s: <WARNING> Not a Z-stack firmware\n", __FUNCTION__);
        version->revision_num = 0;
        version->bootloader_buildtype = 0xFF;
    }
    else
    {
        version->revision_num = SD_READ_NEXT_4_BYTE(reply.data, index_data_reply);
        version->bootloader_buildtype = SD_READ_NEXT_1_BYTE(reply.data, index_data_reply);
    }

    return SD_E_OK;
}

sd_status_e sd_write_osal_nv(struct sd_handle_st* handle, const uint16_t id, const uint8_t offset, const uint8_t len, const uint8_t* data)
{
    struct sd_trame ask   = { 0 };
    struct sd_trame reply = { 0 };
    uint8_t buf[250] = { 0 };

    SD_CHECK_DRIVER(handle);
    SD_CHECK_MANDATORY_ARGUMENT(data);
    SD_CHECK_CAPABILITIES(handle->capabilities, SD_MT_CAP_SYS);

    SET_COMMAND_ID(ask, SYS_OSAL_NV_WRITE);
    SD_WRITE_NEXT_2_BYTE(buf, ask.length, id);
    SD_WRITE_NEXT_1_BYTE(buf, ask.length, offset);
    SD_WRITE_NEXT_1_BYTE(buf, ask.length, len);
    memcpy(&buf[4], data, len);

    ask.length += len;
    ask.data = buf;

    EXCHANGE_TRAME(handle, &ask, &reply, SYS_OSAL_NV_WRITE_ACK, 100);

    debug("%s status = %" PRIu8 "\n", __FUNCTION__, reply.data[0]);

    return SD_CHECK_CMD_STATUS(reply);
}

sd_status_e sd_write_tc_link_key(struct sd_handle_st* handle, const sd_tclinkkey_t key)
{
    return sd_write_osal_nv(handle, ZCD_NV_TC_LINK_KEY, 0, sizeof(sd_tclinkkey_t), key);
}

sd_status_e sd_random(struct sd_handle_st* handle, uint16_t* rand)
{
    struct sd_trame ask   = { 0 };
    struct sd_trame reply = { 0 };

    SD_CHECK_DRIVER(handle);
    SD_CHECK_MANDATORY_ARGUMENT(rand);
    SD_CHECK_CAPABILITIES(handle->capabilities, SD_MT_CAP_SYS);

    SET_COMMAND_ID(ask, SYS_RANDOM);

    ask.length = 0;

    EXCHANGE_TRAME(handle, &ask, &reply, SYS_RANDOM_ACK, 100);

    *rand = SD_READ_2_BYTE(reply.data, 0);

    return SD_E_OK;
}

sd_status_e sd_set_tx_power(struct sd_handle_st* handle, uint8_t* power)
{
    struct sd_trame ask   = { 0 };
    struct sd_trame reply = { 0 };

    SD_CHECK_DRIVER(handle);
    SD_CHECK_MANDATORY_ARGUMENT(power);
    SD_CHECK_CAPABILITIES(handle->capabilities, SD_MT_CAP_SYS);

    SET_COMMAND_ID(ask, SYS_SET_TX_POWER);

    ask.length = 1;
    ask.data = power;

    EXCHANGE_TRAME(handle, &ask, &reply, SYS_SET_TX_POWER_ACK, 100);

    *power = reply.data[0];

    return SD_E_OK;
}
#pragma endregion MT_SYS

#pragma region MT_UTIL
sd_status_e sd_device_info(struct sd_handle_st* handle, struct sd_device_info_st* dev_info)
{
    struct sd_trame ask   = { 0 };
    struct sd_trame reply = { 0 };
    uint8_t index_data_reply = 0;

    SD_CHECK_DRIVER(handle);
    SD_CHECK_MANDATORY_ARGUMENT(dev_info);
    SD_CHECK_CAPABILITIES(handle->capabilities, SD_MT_CAP_UTIL);

    SET_COMMAND_ID(ask, UTIL_GET_DEVICE_INFO);
    ask.length = 0;

    EXCHANGE_TRAME(handle, &ask, &reply, UTIL_GET_DEVICE_INFO_ACK, 100);

    dev_info->status = SD_READ_NEXT_1_BYTE(reply.data, index_data_reply);
    dev_info->IEEEAddr = SD_READ_NEXT_8_BYTE(reply.data, index_data_reply);
    dev_info->shortAddr = SD_READ_NEXT_2_BYTE(reply.data, index_data_reply);
    dev_info->deviceType = SD_READ_NEXT_1_BYTE(reply.data, index_data_reply);
    dev_info->deviceState = SD_READ_NEXT_1_BYTE(reply.data, index_data_reply);

    debug("status : %" PRIu8 "\n", dev_info->status);
    debug("IEEEAddr : %" PRIX64 "\n", dev_info->IEEEAddr);
    debug("shortAddr : %" PRIu16 "\n", dev_info->shortAddr);
    debug("deviceType : %" PRIu8 "\n", dev_info->deviceType);
    debug("deviceState : %" PRIu8 "\n", dev_info->deviceState);

    return SD_E_OK;
}

sd_status_e sd_allow_bind(struct sd_handle_st* handle, const uint8_t timeout)
{
    struct sd_trame ask   = { 0 };
    struct sd_trame reply = { 0 };

    SD_CHECK_DRIVER(handle);
    SD_CHECK_CAPABILITIES(handle->capabilities, SD_MT_CAP_UTIL);

    SET_COMMAND_ID(ask, ZB_ALLOW_BIND);
    ask.length = 1;
    ask.data = &timeout;

    EXCHANGE_TRAME(handle, &ask, &reply, ZB_ALLOW_BIND_ACK, 100);

    return SD_E_OK;
}

sd_status_e sd_set_security_level(struct sd_handle_st* handle, const uint8_t security_level)
{
    struct sd_trame ask   = { 0 };
    struct sd_trame reply = { 0 };

    SD_CHECK_DRIVER(handle);
    SD_CHECK_CAPABILITIES(handle->capabilities, SD_MT_CAP_UTIL);

    SET_COMMAND_ID(ask, UTIL_SET_SECLEVEL);
    ask.length = 1;
    ask.data = &security_level;

    EXCHANGE_TRAME(handle, &ask, &reply, UTIL_SET_SECLEVEL_ACK, 100);

    return SD_CHECK_CMD_STATUS(reply);
}
#pragma endregion MT_UTIL

#pragma region MT_ZDO
sd_status_e sd_request_IEEE_addr(struct sd_handle_st* handle, const uint16_t device, const uint8_t reqType, const uint8_t startIndex)
{
    struct sd_trame ask   = { 0 };
    struct sd_trame reply = { 0 };
    uint8_t buf[4] = { 0 };

    SD_CHECK_DRIVER(handle);
    SD_CHECK_CAPABILITIES(handle->capabilities, SD_MT_CAP_ZDO);

    SET_COMMAND_ID(ask, ZDO_IEEE_ADDR_REQ);

    SD_WRITE_NEXT_2_BYTE(buf, ask.length, device);
    SD_WRITE_NEXT_1_BYTE(buf, ask.length, reqType);
    SD_WRITE_NEXT_1_BYTE(buf, ask.length, startIndex);

    ask.data = buf;

    EXCHANGE_TRAME(handle, &ask, &reply, ZDO_IEEE_ADDR_REQ_ACK, 5000);

    if (reply.data[0] == 0)
    {
        int8_t index_data_reply = 0;

        if (wait_for_trame(handle, &reply, ZDO_IEEE_ADDR_RSP, 10000) != SD_E_OK)
        {
            debug("%s: receive failed\n", __FUNCTION__);
            return SD_E_KO;
        }

        uint8_t status = SD_READ_NEXT_1_BYTE(reply.data, index_data_reply);
        uint64_t IEEEAddr = SD_READ_NEXT_8_BYTE(reply.data, index_data_reply);
        uint16_t nwkAddr = SD_READ_NEXT_2_BYTE(reply.data, index_data_reply);
        uint8_t startIndex2 = SD_READ_NEXT_1_BYTE(reply.data, index_data_reply);
        uint8_t numAssocDev = SD_READ_NEXT_1_BYTE(reply.data, index_data_reply);

        debug("status = %" PRIu8 "\n", status);
        debug("IEEEAddr = %" PRIX64 "\n", IEEEAddr);
        debug("nwkAddr = %" PRIu16 "\n", nwkAddr);
        debug("startIndex = %" PRIu8 "\n", startIndex2);
        debug("numAssocDev = %" PRIu8 "\n", numAssocDev);

        return SD_E_OK;
    }

    return SD_E_KO;
}

sd_status_e sd_bind_request(struct sd_handle_st* handle, uint16_t dstAddr, uint64_t srcAddress, uint8_t srcEndpoint, uint16_t clusterId, uint8_t dstAddrMode, uint64_t dstAddress, uint8_t dstEndpoint)
{
    struct sd_trame ask   = { 0 };
    struct sd_trame reply = { 0 };
    uint8_t buf[0x17] = { 0 };

    SD_CHECK_DRIVER(handle);
    SD_CHECK_CAPABILITIES(handle->capabilities, SD_MT_CAP_ZDO);

    SET_COMMAND_ID(ask, ZDO_BIND_REQ);
    SD_WRITE_NEXT_2_BYTE(buf, ask.length, dstAddr);
    SD_WRITE_NEXT_8_BYTE(buf, ask.length, srcAddress);
    SD_WRITE_NEXT_1_BYTE(buf, ask.length, srcEndpoint);
    SD_WRITE_NEXT_2_BYTE(buf, ask.length, clusterId);
    SD_WRITE_NEXT_1_BYTE(buf, ask.length, dstAddrMode);
    if (dstAddrMode == 0x3)
    {
        SD_WRITE_NEXT_8_BYTE(buf, ask.length, dstAddress);
        SD_WRITE_NEXT_1_BYTE(buf, ask.length, dstEndpoint);
    }
    else
    {
        SD_WRITE_NEXT_2_BYTE(buf, ask.length, dstAddress);
    }

    debug("%s size is %" PRIX8 "\n", __FUNCTION__, ask.length);

    ask.data = buf;

    EXCHANGE_TRAME(handle, &ask, &reply, ZDO_BIND_REQ_ACK, 5000);

    if (reply.data[0] == 0)
    {
        debug("%s ACK\n", __FUNCTION__ );
        if (wait_for_trame(handle, &reply, ZDO_BIND_RSP, 10000) != SD_E_OK)
        {
            debug("%s: receive failed\n", __FUNCTION__);
            return SD_E_KO;
        }

        uint16_t srcAddr = SD_READ_2_BYTE(reply.data, 0);
        uint8_t  status  = SD_READ_1_BYTE(reply.data, 2);

        debug("status = %" PRIu8 "\n", status);
        debug("srcAddr = %" PRIu16 "\n", srcAddr);

        return SD_E_OK;
    }

    // debug("%s status : %d length = %hu\n", __FUNCTION__, reply.data[0], reply.length);
    debug("reply :");
    for (int i = 0; i < reply.length; ++i)
    {
        debug(" %" PRIX8, reply.data[i]);
    }
    debug("\n");

    return SD_E_KO;
}

sd_status_e sd_set_ZDO_callback(struct sd_handle_st* handle, const uint16_t clusterId, uint8_t* status)
{
    struct sd_trame ask   = { 0 };
    struct sd_trame reply = { 0 };
    uint8_t buf[2] = { 0 };

    SD_CHECK_DRIVER(handle);
    SD_CHECK_CAPABILITIES(handle->capabilities, SD_MT_CAP_ZDO);

    SET_COMMAND_ID(ask, ZDO_MSG_CB_REGISTER_REQ);

    SD_WRITE_NEXT_2_BYTE(buf, ask.length, clusterId);

    ask.data = buf;

    EXCHANGE_TRAME(handle, &ask, &reply, ZDO_MSG_CB_REGISTER_REQ_ACK, 100);

    debug("%s status: %d\n", __FUNCTION__, reply.data[0]);

    if (status != NULL)
    {
        *status = reply.data[0];
    }

    return SD_E_OK;
}

sd_status_e sd_discover_networks(struct sd_handle_st* handle, const uint16_t panID, const uint32_t channel, const uint8_t duration)
{
    struct sd_trame ask   = { 0 };
    struct sd_trame reply = { 0 };
    uint8_t buf[8] = { 0 };

    SD_CHECK_DRIVER(handle);
    SD_CHECK_CAPABILITIES(handle->capabilities, SD_MT_CAP_ZDO);

    SET_COMMAND_ID(ask, ZDO_MGMT_NWK_DISC_REQ);

    SD_WRITE_NEXT_2_BYTE(buf, ask.length, panID);
    SD_WRITE_NEXT_4_BYTE(buf, ask.length, channel);
    SD_WRITE_NEXT_1_BYTE(buf, ask.length, duration);
    SD_WRITE_NEXT_1_BYTE(buf, ask.length, 0);

    ask.data = buf;

    EXCHANGE_TRAME(handle, &ask, &reply, ZDO_MGMT_NWK_DISC_REQ_ACK, 5000);

    if (reply.data[0] == 0)
    {
        if (wait_for_trame(handle, &reply, ZDO_MGMT_NWK_DISC_RSP, 65535) != SD_E_OK)
        {
            debug("%s: receive failed\n", __FUNCTION__);
            return SD_E_KO;
        }

        /*uint16_t source_addr = reply.data[0] << 8 | reply.data[1];
        uint8_t  status = reply.data[2];
        uint8_t  network_count = reply.data[3];
        uint8_t  start_index = reply.data[4];
        uint8_t  network_list_count = reply.data[5];

        debug("status = %d\n", status);
        debug("source_addr = %d\n", source_addr);
        debug("network_count = %d\n", network_count);
        debug("start_index = %d\n", start_index);
        debug("network_list_count = %d\n", network_list_count);

        for (uint8_t i=0; i < network_list_count; ++i)
        {
            uint16_t netpanID = reply.data[6 + 0 + (i * 6)] << 8 | reply.data[6 + 1 + (i * 6)];
            uint8_t logical_channel = reply.data[6 + 2 + (i * 6)];
            uint8_t stack_profile = reply.data[6 + 3 + (i * 6)];
            uint8_t beacon_order = reply.data[6 + 4 + (i * 6)];
            uint8_t permit_join = reply.data[6 + 5 + (i * 6)];

            debug("Network %d: panID = 0x%04X\n", i, netpanID);
            debug("Network %d: logical_channel = %d\n", i, logical_channel);
            debug("Network %d: stack_profile = 0x%02X\n", i, stack_profile);
            debug("Network %d: beacon_order = 0x%02X\n", i, beacon_order);
            debug("Network %d: permit_join = %d\n", i, permit_join);
        }*/

        return SD_E_OK;
    }

    debug("merde %d\n", reply.data[0]);

    return SD_E_KO;
}

sd_status_e sd_get_bind_table(struct sd_handle_st* handle, const uint16_t dstAddr, const uint8_t startIndex)
        {
    struct sd_trame ask = {0};
    struct sd_trame reply = {0};
    uint8_t buf[3] = {0};

    SD_CHECK_DRIVER(handle);
    SD_CHECK_CAPABILITIES(handle->capabilities, SD_MT_CAP_ZDO);

    SET_COMMAND_ID(ask, ZDO_MGMT_BIND_REQ);

    SD_WRITE_NEXT_2_BYTE(buf, ask.length, dstAddr);
    SD_WRITE_NEXT_1_BYTE(buf, ask.length, startIndex);

    ask.data = buf;


    EXCHANGE_TRAME(handle, &ask, &reply, ZDO_MGMT_BIND_REQ_ACK, 1000);

    return SD_CHECK_CMD_STATUS(reply);
}

sd_status_e sd_discover_networks2(struct sd_handle_st* handle, const uint32_t channel, const uint8_t duration)
{
    struct sd_trame ask   = { 0 };
    struct sd_trame reply = { 0 };
    uint8_t buf[5] = { 0 };

    SD_CHECK_DRIVER(handle);
    SD_CHECK_CAPABILITIES(handle->capabilities, SD_MT_CAP_ZDO);

    SET_COMMAND_ID(ask, ZDO_NWK_DISCOVERY_REQ);

    SD_WRITE_NEXT_4_BYTE(buf, ask.length, channel);
    SD_WRITE_NEXT_1_BYTE(buf, ask.length, duration);

    ask.data = buf;

    EXCHANGE_TRAME(handle, &ask, &reply, ZDO_NWK_DISCOVERY_REQ_ACK, 5000);

    /*if (reply.data[0] == 0)
    {
        if (wait_for_trame(handle, &reply, ZDO_NWK_DISCOVERY_CNF, 10000) != SD_E_OK)
        {
            debug("%s: receive failed\n", __FUNCTION__);
            return SD_E_KO;
        }

        uint8_t status = reply.data[0];

        debug("%s status=%d\n", __FUNCTION__, status);

        return SD_E_OK;
    }*/

    return SD_CHECK_CMD_STATUS(reply);
}

sd_status_e sd_allow_join(struct sd_handle_st* handle, const uint8_t addrMode, const uint16_t dstAddr, const uint8_t duration, const uint8_t TCSignificance)
{
    struct sd_trame ask   = { 0 };
    struct sd_trame reply = { 0 };
    uint8_t buf[5] = { 0 };

    SD_CHECK_DRIVER(handle);
    SD_CHECK_CAPABILITIES(handle->capabilities, SD_MT_CAP_ZDO);

    SET_COMMAND_ID(ask, ZDO_MGMT_PERMIT_JOIN_REQ);

    SD_WRITE_NEXT_1_BYTE(buf, ask.length, addrMode);
    SD_WRITE_NEXT_2_BYTE(buf, ask.length, dstAddr);
    SD_WRITE_NEXT_1_BYTE(buf, ask.length, duration);
    SD_WRITE_NEXT_1_BYTE(buf, ask.length, TCSignificance);

    ask.data = buf;

    EXCHANGE_TRAME(handle, &ask, &reply, ZDO_MGMT_PERMIT_JOIN_REQ_ACK, 100);

    if (reply.data[0] == 0)
    {
        if (wait_for_trame(handle, &reply, ZDO_MGMT_PERMIT_JOIN_RSP, 10000) != SD_E_OK)
        {
            debug("%s: receive failed\n", __FUNCTION__);
            return SD_E_KO;
        }

        uint16_t srcAddr = SD_READ_2_BYTE(reply.data, 0);
        uint8_t status = SD_READ_1_BYTE(reply.data, 2);

        debug("%s srcAddr=%hX\n", __FUNCTION__, srcAddr);
        debug("%s status=%d\n", __FUNCTION__, status);

        return SD_E_OK;
    }

    return SD_E_KO;
}

sd_status_e sd_startup_from_app(struct sd_handle_st* handle, const uint16_t delay)
{
    struct sd_trame ask   = { 0 };
    struct sd_trame reply = { 0 };
    uint8_t buf[2] = { 0 };

    SD_CHECK_DRIVER(handle);
    SD_CHECK_CAPABILITIES(handle->capabilities, SD_MT_CAP_ZDO);

    SET_COMMAND_ID(ask, ZDO_STARTUP_FROM_APP);

    SD_WRITE_NEXT_2_BYTE(buf, ask.length, delay);

    ask.data = buf;

    EXCHANGE_TRAME(handle, &ask, &reply, ZDO_STARTUP_FROM_APP_ACK, delay * 1000 + 1000);

    debug("startup status: %d\n", reply.data[0]);

    return SD_E_OK;
}

sd_status_e sd_startup_from_app_ex(struct sd_handle_st* handle, const uint8_t delay, const uint8_t mode)
{
    struct sd_trame ask   = { 0 };
    struct sd_trame reply = { 0 };
    uint8_t buf[2] = { 0 };

    SD_CHECK_DRIVER(handle);
    SD_CHECK_CAPABILITIES(handle->capabilities, SD_MT_CAP_ZDO);

    SET_COMMAND_ID(ask, ZDO_STARTUP_FROM_APP_EX);

    SD_WRITE_NEXT_1_BYTE(buf, ask.length, delay);
    SD_WRITE_NEXT_1_BYTE(buf, ask.length, mode);

    ask.data = buf;

    EXCHANGE_TRAME(handle, &ask, &reply, ZDO_STARTUP_FROM_APP_ACK, delay * 1000 + 1000);

    debug("startup status: %d\n", reply.data[0]);

    return SD_E_OK;
}

void sd_evt_af_incoming_msg(struct sd_handle_st* handle, const struct sd_trame* callback)
{
    uint32_t index_data_reply = 0;

}

void sd_evt_mgm_bind(struct sd_handle_st* handle, const struct sd_trame* callback)
{
    uint32_t index_data_reply = 0;
    const uint16_t srcAddr = SD_READ_NEXT_2_BYTE(callback->data, index_data_reply);
    const uint8_t status = SD_READ_NEXT_1_BYTE(callback->data, index_data_reply);
    const uint8_t bindTableEntry = SD_READ_NEXT_1_BYTE(callback->data, index_data_reply);
    const uint8_t startIndex = SD_READ_NEXT_1_BYTE(callback->data, index_data_reply);
    const uint8_t bindTableListCount = SD_READ_NEXT_1_BYTE(callback->data, index_data_reply);

    debug("%s srcAddr = %" PRIu16 "\n", __FUNCTION__, srcAddr);
    debug("%s status = %" PRIu8 "\n", __FUNCTION__, status);
    debug("%s bindTableEntry = %" PRIu8 "\n", __FUNCTION__, bindTableEntry);
    debug("%s startIndex = %" PRIu8 "\n", __FUNCTION__, startIndex);
    debug("%s bindTableListCount = %" PRIu8 "\n", __FUNCTION__, bindTableListCount);

    for (uint8_t i=0; i < bindTableListCount; i++)
    {
        const uint64_t bindSrcAddr = SD_READ_NEXT_8_BYTE(callback->data, index_data_reply);
        const uint8_t bindEndpoint = SD_READ_NEXT_1_BYTE(callback->data, index_data_reply);
        const uint8_t bindClusterId = SD_READ_NEXT_1_BYTE(callback->data, index_data_reply);
        const uint8_t bindDstAddrMode = SD_READ_NEXT_1_BYTE(callback->data, index_data_reply);
        const uint64_t bindDstAddr = SD_READ_NEXT_8_BYTE(callback->data, index_data_reply);
        const uint8_t bindDstEndpoint = SD_READ_NEXT_1_BYTE(callback->data, index_data_reply);

        debug("%s bind[%" PRIu8 "] bindSrcAddr = %" PRIX64 "\n", __FUNCTION__, i, bindSrcAddr);
        debug("%s bind[%" PRIu8 "] bindEndpoint = %" PRIu16 "\n", __FUNCTION__, i, bindEndpoint);
        debug("%s bind[%" PRIu8 "] bindClusterId = %" PRIu16 "\n", __FUNCTION__, i, bindClusterId);
        debug("%s bind[%" PRIu8 "] bindDstAddrMode = %" PRIu16 "\n", __FUNCTION__, i, bindDstAddrMode);
        debug("%s bind[%" PRIu8 "] bindDstAddr = %" PRIX64 "\n", __FUNCTION__, i, bindDstAddr);
        debug("%s bind[%" PRIu8 "] bindDstEndpoint = %" PRIu16 "\n", __FUNCTION__, i, bindDstEndpoint);
    }
}


void sd_evt_zdo_state_change(struct sd_handle_st* handle, const struct sd_trame* trame)
{
    const uint8_t state = trame->data[0];

    debug("ZDO state %s (0x%02X)\n", get_name_from_id(zdo_status, state), state);
}

void sd_evt_zdo_nwk_discovery(struct sd_handle_st* handle, const struct sd_trame* trame)
{
    const uint8_t status = trame->data[0];

    debug("Network discovery cnf %s (0x%02X)\n", get_name_from_id(nwk_discovery_status, status), status);
}

void sd_evt_zdo_msg_cb_incoming(struct sd_handle_st* handle, const struct sd_trame* trame)
{
    uint32_t index_data_reply = 0;

    const uint16_t srcAddr = SD_READ_NEXT_2_BYTE(trame->data, index_data_reply);
    const uint8_t wasBroadcast = SD_READ_NEXT_1_BYTE(trame->data, index_data_reply);
    const uint16_t clusterId = SD_READ_NEXT_2_BYTE(trame->data, index_data_reply);

    debug("%s srcAddr = %" PRIX16 "\n", __FUNCTION__, srcAddr);
    debug("%s wasBroadcast = %" PRIu8 "\n", __FUNCTION__, wasBroadcast);
    debug("%s clusterId = %" PRIu16 "\n", __FUNCTION__, clusterId);

    // The fuck ?
    // Pas d'autre header a parser, on passe direct aux data.
    // Par contre le premier octet se retrouve perdu au millieu de 3 autre octets de 0...
    if (clusterId == 0)
    {
        uint8_t buffer[128] = {0};
        uint8_t ll = 1;
        const uint32_t garbage = SD_READ_NEXT_4_BYTE(trame->data, index_data_reply);
        const uint8_t firstOctet = (garbage >> 8);
        buffer[0] = firstOctet;
        debug("%s Data: %02" PRIX8, __FUNCTION__, firstOctet);
        for (uint32_t i = index_data_reply; i < trame->length; ++i)
        {
            debug(" %02" PRIX8, trame->data[i]);
            buffer[ll] = trame->data[i];
            ll += 1;
        }
        debug("\n");
        sd_send_data_to_endpoint(handle, srcAddr, 0, 0, clusterId, 0, 0x21, 0x2, ll, buffer);
    }
    else
    {
        const uint8_t securityUse = SD_READ_NEXT_1_BYTE(trame->data, index_data_reply);
        const uint8_t seqNum = SD_READ_NEXT_1_BYTE(trame->data, index_data_reply);
        const uint16_t macDstAddr = SD_READ_NEXT_2_BYTE(trame->data, index_data_reply);

        debug("%s securityUse = %" PRIu8 "\n", __FUNCTION__, securityUse);
        debug("%s seqNum = %" PRIu8 "\n", __FUNCTION__, seqNum);
        debug("%s macDstAddr = %" PRIX16 "\n", __FUNCTION__, macDstAddr);
    }

}

void sd_evt_zdo_beacon_notify(struct sd_handle_st* handle, const struct sd_trame* trame)
{
    uint32_t index_data_reply = 0;
    uint8_t beaconCount = SD_READ_NEXT_1_BYTE(trame->data, index_data_reply);
    debug("%s found %d beacons\n",__FUNCTION__, beaconCount);
    for (uint8_t n=0; n < beaconCount; ++n)
    {
        uint16_t source_addr = SD_READ_NEXT_2_BYTE(trame->data, index_data_reply);
        uint16_t panID = SD_READ_NEXT_2_BYTE(trame->data, index_data_reply);
        uint8_t logical_chanel = SD_READ_NEXT_1_BYTE(trame->data, index_data_reply);
        uint8_t permit_join = SD_READ_NEXT_1_BYTE(trame->data, index_data_reply);
        uint8_t router_capacity = SD_READ_NEXT_1_BYTE(trame->data, index_data_reply);
        uint8_t device_capacity = SD_READ_NEXT_1_BYTE(trame->data, index_data_reply);
        uint8_t protocol_version = SD_READ_NEXT_1_BYTE(trame->data, index_data_reply);
        uint8_t stack_profile = SD_READ_NEXT_1_BYTE(trame->data, index_data_reply);
        uint8_t lqi = SD_READ_NEXT_1_BYTE(trame->data, index_data_reply);
        uint8_t depth = SD_READ_NEXT_1_BYTE(trame->data, index_data_reply);
        uint8_t update_id = SD_READ_NEXT_1_BYTE(trame->data, index_data_reply);

        uint64_t extPanID = SD_READ_NEXT_8_BYTE(trame->data, index_data_reply);

        debug("%s Beacon %" PRIu8 "\n", __FUNCTION__, n);
        debug("%s source_addr = %" PRIX16 "\n", __FUNCTION__, source_addr);
        debug("%s panID = %04" PRIX16 "\n", __FUNCTION__, panID);
        debug("%s logical_chanel = %" PRIu8 "\n", __FUNCTION__, logical_chanel);
        debug("%s permit_join = %" PRIu8 "\n", __FUNCTION__, permit_join);
        debug("%s router_capacity = %" PRIu8 "\n", __FUNCTION__, router_capacity);
        debug("%s device_capacity = %" PRIu8 "\n", __FUNCTION__, device_capacity);
        debug("%s protocol_version = %" PRIu8 "\n", __FUNCTION__, protocol_version);
        debug("%s stack_profile = %" PRIu8 "\n", __FUNCTION__, stack_profile);
        debug("%s lqi = %" PRIu8 "\n", __FUNCTION__, lqi);
        debug("%s depth = %" PRIu8 "\n", __FUNCTION__, depth);
        debug("%s update_id = %" PRIu8 "\n", __FUNCTION__, update_id);
        debug("%s extPanID = %08" PRIX64 "\n", __FUNCTION__, extPanID);
    }
}

void sd_evt_zdo_concentrator_ind(struct sd_handle_st* handle, const struct sd_trame* trame)
{
    uint8_t index_data_reply = 0;
    const uint16_t srcAddr = SD_READ_NEXT_2_BYTE(trame->data, index_data_reply);
    const uint64_t IEEEAddr = SD_READ_NEXT_8_BYTE(trame->data, index_data_reply);
    const uint8_t cost = SD_READ_NEXT_1_BYTE(trame->data, index_data_reply);

    debug("%s srcAddr = %" PRIX16 "\n", __FUNCTION__, srcAddr);
    debug("%s IEEEAddr = %" PRIX64 "\n", __FUNCTION__, IEEEAddr);
    debug("%s cost = %" PRIX8 "\n", __FUNCTION__, cost);
}

void sd_evt_zdo_dev_annce_ind(struct sd_handle_st* handle, const struct sd_trame* trame)
{
    uint8_t index_data_reply = 0;
    const uint16_t srcAddr = SD_READ_NEXT_2_BYTE(trame->data, index_data_reply);
    const uint16_t nwkAddr = SD_READ_NEXT_2_BYTE(trame->data, index_data_reply);
    const uint64_t IEEEAddr = SD_READ_NEXT_8_BYTE(trame->data, index_data_reply);
    const uint8_t capabilities = SD_READ_NEXT_1_BYTE(trame->data, index_data_reply);

    debug("%s srcAddr = %" PRIX16 "\n", __FUNCTION__, srcAddr);
    debug("%s nwkAddr = %" PRIX16 "\n", __FUNCTION__, nwkAddr);
    debug("%s IEEEAddr = %" PRIX64 "\n", __FUNCTION__, IEEEAddr);
    debug("%s capabilities = %" PRIX16 "\n", __FUNCTION__, capabilities);
    debug("%s capabilities[0] = alternate PAN coordinator : %s\n", __FUNCTION__, ((capabilities & 0x01) ? "true" : "false"));
    debug("%s capabilities[1] = device type : %s\n", __FUNCTION__, ((capabilities & 0x02) ? "router" : "end device"));
    debug("%s capabilities[2] = power source : %s\n", __FUNCTION__, ((capabilities & 0x04) ? "main powered" : "???"));
    debug("%s capabilities[3] = receiver ON when IDLE : %s\n", __FUNCTION__, ((capabilities & 0x08) ? "yes" : "no"));
    debug("%s capabilities[6] = security capabilities : %s\n", __FUNCTION__, ((capabilities & 0x40) ? "true" : "false"));
}

void sd_evt_zdo_src_rtg_ind(struct sd_handle_st* handle, const struct sd_trame* trame)
{
    uint8_t index_data_reply = 0;
    const uint16_t dstAddr = SD_READ_NEXT_2_BYTE(trame->data, index_data_reply);
    const uint8_t relayCount = SD_READ_NEXT_1_BYTE(trame->data, index_data_reply);

    debug("%s dstAddr = %" PRIX16 "\n", __FUNCTION__, dstAddr);
    for (uint8_t i=0; i < relayCount; ++i)
    {
        const uint16_t relayList = SD_READ_NEXT_2_BYTE(trame->data, index_data_reply);
        debug("%s relayList[%" PRIu8 "] = %" PRIX16 "\n", __FUNCTION__, i, relayList);
    }
}

void sd_evt_zdo_dev_ind(struct sd_handle_st* handle, const struct sd_trame* trame)
{
    uint8_t index_data_reply = 0;
    const uint16_t srcNwkAddr = SD_READ_NEXT_2_BYTE(trame->data, index_data_reply);
    const uint64_t IEEESource = SD_READ_NEXT_8_BYTE(trame->data, index_data_reply);
    const uint16_t parentNwkAddr = SD_READ_NEXT_2_BYTE(trame->data, index_data_reply);

    debug("%s srcNwkAddr = %" PRIX16 "\n", __FUNCTION__, srcNwkAddr);
    debug("%s IEEESource = %" PRIX64 "\n", __FUNCTION__, IEEESource);
    debug("%s parentNwkAddr = %" PRIX16 "\n", __FUNCTION__, parentNwkAddr);
}
#pragma endregion MT_ZDO

#pragma region MT_APPCONFIG
sd_status_e sd_trust_center_allow_join(struct sd_handle_st* handle, const bool allow)
{
    struct sd_trame ask   = { 0 };
    struct sd_trame reply = { 0 };

    uint8_t bAllow = (allow) ? 1 : 0;

    SD_CHECK_DRIVER(handle);
    SD_CHECK_CAPABILITIES(handle->capabilities, SD_MT_CAP_UTIL);

    SET_COMMAND_ID(ask, APP_CNF_SET_ALLOWREJOIN_TC_POLICY);
    ask.length = 1;
    ask.data = &bAllow;

    EXCHANGE_TRAME(handle, &ask, &reply, APP_CNF_SET_ALLOWREJOIN_TC_POLICY_ACK, 100);

    return SD_CHECK_CMD_STATUS(reply);
}

sd_status_e sd_commissioning(struct sd_handle_st* handle, const uint8_t commissioning_mode)
{
    struct sd_trame ask   = { 0 };
    struct sd_trame reply = { 0 };

    SD_CHECK_DRIVER(handle);
    SD_CHECK_CAPABILITIES(handle->capabilities, SD_MT_CAP_UTIL);

    SET_COMMAND_ID(ask, APP_CNF_BDB_START_COMMISSIONING);
    ask.length = 1;
    ask.data = &commissioning_mode;

    EXCHANGE_TRAME(handle, &ask, &reply, APP_CNF_BDB_START_COMMISSIONING_ACK, 5000);

    if (reply.data[0] == 0)
    {
        if (wait_for_trame(handle, &reply, APP_CNF_BDB_COMMISSIONING_NOTIFICATION, 10000) != SD_E_OK)
        {
            debug("%s: receive failed\n", __FUNCTION__);
            return SD_E_KO;
        }

        sd_evt_commissioning(handle, &reply);

        return SD_E_OK;
    }

    return SD_E_KO;
}

sd_status_e sd_set_channel(struct sd_handle_st* handle, const bool isPrimary, const uint32_t channel)
{
    struct sd_trame ask   = { 0 };
    struct sd_trame reply = { 0 };
    uint8_t buf[5] = { 0 };

    SD_CHECK_DRIVER(handle);
    SD_CHECK_CAPABILITIES(handle->capabilities, SD_MT_CAP_UTIL);

    SET_COMMAND_ID(ask, APP_CNF_BDB_SET_CHANNEL);

    SD_WRITE_NEXT_1_BYTE(buf, ask.length, (isPrimary) ? 1 : 0);
    SD_WRITE_NEXT_4_BYTE(buf, ask.length, channel);

    ask.data = buf;

    EXCHANGE_TRAME(handle, &ask, &reply, APP_CNF_BDB_SET_CHANNEL_ACK, 10000);

    return SD_CHECK_CMD_STATUS(reply);
}

sd_status_e sd_set_require_key_exchange(struct sd_handle_st* handle, const bool require)
{
    struct sd_trame ask   = { 0 };
    struct sd_trame reply = { 0 };
    const uint8_t raw_require = (require) ? 1 : 0;

    SD_CHECK_DRIVER(handle);
    SD_CHECK_CAPABILITIES(handle->capabilities, SD_MT_CAP_UTIL);

    SET_COMMAND_ID(ask, APP_CNF_BDB_SET_TC_REQUIRE_KEY_EXCHANGE);
    ask.length = 1;
    ask.data = &raw_require;

    EXCHANGE_TRAME(handle, &ask, &reply, APP_CNF_BDB_SET_TC_REQUIRE_KEY_EXCHANGE_ACK, 10000);

    return SD_CHECK_CMD_STATUS(reply);
}

sd_status_e sd_try_rejoin_previous_nwk(struct sd_handle_st* handle)
{
    struct sd_trame ask   = { 0 };
    struct sd_trame reply = { 0 };

    SD_CHECK_DRIVER(handle);
    SD_CHECK_CAPABILITIES(handle->capabilities, SD_MT_CAP_UTIL);

    SET_COMMAND_ID(ask, APP_CNF_BDB_ZED_ATTEMPT_RECOVER_NWK);

    EXCHANGE_TRAME(handle, &ask, &reply, APP_CNF_BDB_ZED_ATTEMPT_RECOVER_NWK_ACK, 10000);

    return SD_CHECK_CMD_STATUS(reply);
}

void sd_evt_commissioning(struct sd_handle_st* handle, const struct sd_trame* trame)
{
    const uint8_t status = trame->data[0];
    const uint8_t mode = trame->data[1];
    const uint8_t remaining = trame->data[2];

#if SD_DEV_MODE
    debug("Commissioning mode %s (0x%02X); status %s (0x%02X); Remaining modes : 0x%02X\n",
          get_name_from_id(commissioning_name, mode), mode,
          get_name_from_id(commissioning_status, status), status,
          remaining);
#endif

    if (remaining != 0)
    {
        //debug("Starting commissioning mode %s\n", get_name_from_id(commissioning_mode_name, remaining));
        //sd_commissioning(handle, remaining);
    }
}
#pragma endregion MT_APPCONFIG

#if SD_DEV_MODE

const char* get_name_from_id(const struct sd_debug_id_st* tab, const uint16_t id)
{
    uint32_t i = 0;
    while (tab[i].cmdName != NULL)
    {
        //debug("%d == %d\n", tab[i].cmdId, id);
        if (tab[i].cmdId == id)
        {
            return tab[i].cmdName;
        }
        i += 1;
    }

    return "unknown";
}

const char* sd_get_cmd_name(const struct sd_trame* cmd)
{
    const uint16_t cmdId = GET_COMMAND_ID(*cmd);
    if (cmd == NULL)
    {
        return "(null)";
    }

    return get_name_from_id(cmd_name, cmdId);
}

#endif
