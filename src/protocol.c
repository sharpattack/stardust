//
// Created by madahin on 29/10/23.
//

#include "protocol.h"

#ifdef __ARM_ARCH_7M__
#include "core_cm4.h"
#endif

static uint8_t gl_trame_buffer[MAX_TRAME_SIZE] = {0};

static uint8_t compute_end_of_frame(const uint8_t*, uint16_t);
static void set_end_of_frame(uint8_t*, uint16_t);

static uint16_t receive(struct sd_handle_st* handle, uint8_t* buffer, uint16_t requested_size)
{
    uint8_t try = 0;
    uint16_t byte_read = 0;
    do
    {
        byte_read += handle->_driver.read(handle, buffer + byte_read, requested_size - byte_read, &handle->status);
        if (byte_read < requested_size)
        {
            try += 1;
            handle->_driver.sleep(5);
        }
    } while ((byte_read < requested_size) && try < 100);

    return byte_read;
}


sd_status_e decode_trame(struct sd_trame* reply, const uint8_t* buf)
{
    if (buf[0] != START_OF_FRAME)
    {
        debug("%s: invalid start of trame\n", __FUNCTION__);
        return SD_E_KO;
    }

    const uint16_t crc_offset = 1 /* start_of_trame */ + 3 /* header */ + buf[1] /* length */;
    const uint8_t crc = buf[crc_offset];
    const uint8_t computed_crc = compute_end_of_frame(buf, crc_offset);

    if (crc != computed_crc)
    {
        debug("%s: invalid CRC. Received 0x%02" PRIX8 ", computed 0x%02" PRIX8 "\n", __FUNCTION__, crc, computed_crc);
        //return SD_E_KO;
    }

    reply->length = buf[1];
    reply->cmd0   = buf[2];
    reply->cmd1   = buf[3];
    if (reply->length)
    {
        reply->data = &(buf[1 + 3]);
    }
    else
    {
        reply->data = NULL;
    }

    return SD_E_OK;
}

sd_status_e send_trame(struct sd_handle_st* handle, const struct sd_trame* trame_ask)
{
    const uint16_t trame_size = 4 /* header */ + trame_ask->length + 1 /* CRC */;
    gl_trame_buffer[0] = START_OF_FRAME;
    gl_trame_buffer[1] = trame_ask->length;
    gl_trame_buffer[2] = trame_ask->cmd0;
    gl_trame_buffer[3] = trame_ask->cmd1;
    if (trame_ask->length)
    {
#ifdef __ARM_ARCH_7M__
        orderedCpy(gl_trame_buffer + 4, trame_ask->data, trame_ask->length);
#else
        memcpy(gl_trame_buffer + 4, trame_ask->data, trame_ask->length);
#endif
    }
    set_end_of_frame(gl_trame_buffer, trame_size);

    uint16_t byte_writen = handle->_driver.write(handle, gl_trame_buffer, trame_size, &handle->status);
    if (byte_writen != trame_size)
    {
        debug("%s: Unable to send all trame bytes (%" PRIu16 "/%" PRIu16 ")\n", __FUNCTION__, byte_writen, trame_size);
        return SD_E_KO;
    }

    return SD_E_OK;
}

sd_status_e receive_trame(struct sd_handle_st* handle, struct sd_trame* trame_reply)
{
    uint16_t byte_read = receive(handle, gl_trame_buffer, HEADER_SIZE);
    // Receiving header
    if (byte_read != HEADER_SIZE)
    {
        debug("%s: Unable to receive all header bytes (%" PRIu16 "/%" PRIu16 ")\n", __FUNCTION__, byte_read, HEADER_SIZE);
        return SD_E_KO;
    }
    // Receiving data if needed
    if (gl_trame_buffer[1])
    {
        byte_read = receive(handle, gl_trame_buffer + HEADER_SIZE, gl_trame_buffer[1]);
        if (byte_read != gl_trame_buffer[1])
        {
            debug("%s: Unable to receive all data bytes (%" PRIu16 "/%" PRIu16 ")\n", __FUNCTION__, byte_read, gl_trame_buffer[1]);
            return SD_E_KO;
        }
    }
    // Receiving CRC
    byte_read = receive(handle, gl_trame_buffer + HEADER_SIZE + gl_trame_buffer[1], CRC_SIZE);
    if (byte_read != CRC_SIZE)
    {
        debug("%s: Unable to receive all CRC bytes (%" PRIu16 "/%" PRIu16 ")\n", __FUNCTION__, byte_read, CRC_SIZE);
        return SD_E_KO;
    }

    return decode_trame(trame_reply, gl_trame_buffer);
}

sd_status_e exchange_trame(struct sd_handle_st* handle, struct sd_trame* trame_reply, const struct sd_trame* trame_ask)
{
    if (send_trame(handle, trame_ask) == SD_E_KO)
    {
        return SD_E_KO;
    }

    for (uint8_t i=0; (i < 100) && !(handle->_driver.available(handle)); ++i)
    {
        handle->_driver.sleep(10);
    }

    return receive_trame(handle, trame_reply);
}

sd_status_e wait_for_trame(struct sd_handle_st* handle, struct sd_trame* trame_reply, const uint16_t trame_id, const uint16_t timeout)
{
    sd_status_e status = SD_E_KO;
    bool infinite_wait = timeout == UINT16_MAX;
    for (uint8_t i=0; (i < 100) && !(handle->_driver.available(handle)); ++i)
    {
        handle->_driver.sleep(10);
    }

    uint16_t i = 0;
    do
    {
        if (handle->_driver.available(handle))
        {
            if (receive_trame(handle, trame_reply) == SD_E_OK)
            {
                const uint16_t received_trame_id = trame_reply->cmd0 << 8 | trame_reply->cmd1;
                if (received_trame_id == trame_id)
                {
                    status = SD_E_OK;
                    break;
                }
                else
                {
                    debug("Wanted trame 0x%02" PRIX16 ", got 0x%02" PRIX16 "\n", trame_id, received_trame_id);
                    sd_update_(handle, trame_reply);
                }
            }
        }
        handle->_driver.sleep(10);
        i += 1;
    } while (infinite_wait || (i < (timeout / 10)));

    return status;
}



static uint8_t compute_end_of_frame(const uint8_t* buf, uint16_t len)
{
    uint8_t ret = 0;
    for (uint16_t i=1; i < len; ++i)
    {
        ret ^= buf[i];
    }
    return ret;
}

static void set_end_of_frame(uint8_t* buf, const uint16_t len)
{
    uint8_t ret = 0;
    for (uint16_t i=1; i < len-1; ++i)
    {
        ret ^= buf[i];
    }
    buf[len-1] = ret;
}

#if SD_DEV_MODE
const char* get_system_name(const uint8_t cmd0)
{
    switch ((cmd0 >> 4) & 0x0F)
    {
        case SYS_POLL: return "POLL";
        case SYS_SREQ: return "SREQ";
        case SYS_AREQ: return "AREQ";
        case SYS_SRSP: return "SRSP";
        default: return "unknown";
    }
}

const char* get_subsystem_name(const uint8_t cmd0)
{
    switch (cmd0 & 0x0F)
    {
        case SUB_RES        : return "reserved";
        case SUB_SYS        : return "SYS interface";
        case SUB_MAC        : return "MAC interface";
        case SUB_NWK        : return "NWK interface";
        case SUB_AF         : return "AF interface";
        case SUB_ZDO        : return "ZDO interface";
        case SUB_SAPI       : return "SAPI interface";
        case SUB_UTIL       : return "UTIL interface";
        case SUB_DEBUG      : return "DEBUG interface";
        case SUB_APP_INT    : return "APP interface";
        case SUB_APP_CONF   : return "APP config";
        case SUB_GREENPOWER : return "GreenPower";
        default             : return "unknown";
    }
}

const char* get_reset_reason(const uint8_t reason)
{
    switch (reason)
    {
        case 0x00        : return "Power-up";
        case 0x01        : return "External";
        case 0x02        : return "Watch-dog";
        default          : return "unknown";
    }
}
#endif